from GameFrame import Level
from Objects import BackButton
import os


class About(Level):
    def __init__(self, screen, joysticks):
        Level.__init__(self, screen, joysticks)

        self.set_background_image(os.path.join("InfoPage", "AbtBack.png"))

        self.back_button = BackButton(self, 500, 550, 24 * 30)
        self.add_room_object(self.back_button)

        narration = self.load_sound("about.ogg")
        narration.play()


