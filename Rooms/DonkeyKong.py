from GameFrame import Level, Globals, EnumLevels, TextObject
from Objects import DK_Boulder, Dirtdk, DK_Block, DK_Tree, DK_Stone, Playerdk, HealthBar
from Objects import CollectibleDK, BackgroundScroll, AnimalDoor,MountainPeak
import os
import random


class DonkeyKong(Level):
    def __init__(self, screen, joysticks):
        Level.__init__(self, screen, joysticks)

        Globals.icon_bar_length = 40
        Globals.next_level = EnumLevels.StoryEnd

        self.Boulder_image = self.load_image((os.path.join('DK', 'Boulder.png')))
        self.set_timer(15, self.add_boulder)
        self.collectible_sound = self.load_sound("Collectible.wav")
        self.room_items = []

        self.Player = Playerdk(self, 640, Globals.SCREEN_HEIGHT - 150)
        self.add_room_object(self.Player)

        self.healthBar = HealthBar(self, 1050, 32)
        self.add_room_object(self.healthBar)

        self.background_sound = self.load_sound('Background.wav')
        self.background_sound.set_volume(0.2)
        self.background_sound.play(-1)


        # - Set up maze, objects 32x32 - #
        room_objects = [
            'P                                                 '
            '                                       S          ',
            '                                                  ',
            '                                                  ',
            '                                                  ',
            '                                                  ',
            '             S                                    ',
            '                                                  ',
            '                                       T          ',
            '                                                  ',
            '                                                  ',
            '                                                  ',
            '                                                  ',
            '                                                  ',
            '                       T                          ',
            '                                                  ',
            '                      F                           ',
            '                                                  ',
            '                                                  ',
            '             S                                    ',
            '                                                  ',
            '                                        S         ',
            '                                                  ',
            '                                                  ',
            '                                                  ',
            '                                                   ',
            '                                                  ',
            '      T                                           ',
            '                                                  ',
            '                                                  ',
            '                            S                     ',
            '                                                  ',
            '                                                  ',
            '                                                  ',
            '            T                                     ',
            '                                                  ',
            '                                     T            ',
            '                                                  ',
            '                                                  ',
            '                                                  ',
            '                                                  ',
            '          s                                       ',
            '                                                  ',
            '                                                  ',
            '                                                  ',
            '                            T                     ',
            '                                                  ',
            '                                                  ',
            '                                                  ',
            '                                                  ',
            '                       S                          ',
            '        T                                         ',
            '                                                  ',
            '                                                  ',
            '                                T                 ',
            '                                                  ',
            '                                                  ',
            '                                                  ',
            '                  S                               ',
            '                                          S       ',
            '                                                  ',
            '                                                  ',
            '                                                  ',
            '         T                                        ',
            '                                                  ',
            '                                                  ',
            '                                                  ',
            '                                                  ',
            '                                                  ',
            '                                                  ',
            '                                                  ',
            'BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB'
        ]
        self.animal_door = AnimalDoor(self, 0, 0)
        self.bg1 = BackgroundScroll(self, 0, 0)
        self.bg2 = BackgroundScroll(self, 0, - Globals.SCREEN_HEIGHT)
        self.add_room_object(self.bg1)
        self.add_room_object(self.bg2)

        for i, row in enumerate(room_objects):
            for j, obj in enumerate(row):
                if obj == 'P':
                    peak = MountainPeak(self, j * 32, i * 32 - 260)
                    self.add_room_object(peak)
                    self.room_items.append(peak)
                elif obj == 'B':
                    new_block = DK_Block(self, j * 32, i * 32)
                    self.add_room_object(new_block)
                    self.room_items.append(new_block)
                elif obj == 'D':
                    new_dirt = Dirtdk(self, j * 32, i * 32)
                    self.add_room_object(new_dirt)
                    self.room_items.append(new_dirt)
                elif obj == 'T':
                    new_tree = DK_Tree(self, j * 32, i * 32)
                    self.add_room_object(new_tree)
                    self.room_items.append(new_tree)
                elif obj == 'S':
                    new_stone = DK_Stone(self, j * 32, i * 32)
                    self.add_room_object(new_stone)
                    self.room_items.append(new_stone)
                #elif obj == 'F':
                #    self.animal_door.x = j * 32
                #    self.animal_door.y = i * 32
                #    self.add_room_object(self.animal_door)

        for item in self.room_items:
            item.y -= 1550

        self.room_items.append(self.bg1)
        self.room_items.append(self.bg2)

        self.animal_text = TextObject(self, 25, 100, f"Collected {Globals.animals} of 5 Animals", size=20)
        self.animal_text.depth = 1000
        self.animal_text.colour = (255, 255, 255)
        self.animal_text.update_text()
        self.add_room_object(self.animal_text)

        flora_fauna = list()
        flora_fauna.append(CollectibleDK(self, 0, 0, 'turkey'))
        flora_fauna.append(CollectibleDK(self, 0, 34, 'cassowary'))
        flora_fauna.append(CollectibleDK(self, 0, 68, 'crocodile'))
        flora_fauna.append(CollectibleDK(self, 0, 102, 'mullet'))
        flora_fauna.append(CollectibleDK(self, 0, 136, 'turtle'))

        for item in flora_fauna:
            unique_spot = False
            while not unique_spot:
                item.x = random.randint(64, Globals.SCREEN_WIDTH - 64)
                item.y = random.randint(-1000, Globals.SCREEN_HEIGHT - 200)
                # check x and y against all other objects
                # if none in same area, set unique_spot to True
                collision_found = False
                for other in self.room_items:
                    if other is not item:
                        other_type = type(other).__name__
                        # if other_type != 'DK_Tree' and other_type != 'DK_Block' :
                        #     if abs(other.x - item.x) < 32 and abs(other.y - item.y) < 32: # other.rect.colliderect(item.rect):
                        #         collision_found = True
                        #         break
                        # else:
                        if item.y < other.y + 250 and \
                                item.y + 32 > other.y and \
                                item.x < other.x + 202 and \
                                item.x + 32 > other.x:
                            collision_found = True
                            break
                if not collision_found:
                    unique_spot = True

            self.add_room_object(item)
            self.room_items.append(item)

    def add_boulder(self):
        if Globals.num_Boulders < 4:
            Globals.num_Boulders += 1
            self.add_room_object(DK_Boulder(self, 0, 0, self.Boulder_image, 10, 25))
            self.set_timer(1, self.add_boulder)

    def times_up(self):
        Globals.num_Boulders = 0
        self.running = False

    def shift_room_left(self):
        for item in self.room_items:
            item.x -= Globals.move_speed

    def shift_room_right(self):
        for item in self.room_items:
            item.x += Globals.move_speed

    def shift_room_up(self):
        for item in self.room_items:
            item.y -= Globals.move_speed
        if self.bg1.y <= - Globals.SCREEN_HEIGHT:
            self.bg1.y = self.bg1.y + Globals.SCREEN_HEIGHT * 2
        if self.bg2.y <= - Globals.SCREEN_HEIGHT:
            self.bg2.y = self.bg2.y + Globals.SCREEN_HEIGHT * 2

    def shift_room_down(self):
        for item in self.room_items:
            item.y += Globals.move_speed
        if self.bg1.y >= Globals.SCREEN_HEIGHT:
            self.bg1.y = self.bg1.y - Globals.SCREEN_HEIGHT * 2
        if self.bg2.y >= Globals.SCREEN_HEIGHT:
            self.bg2.y = self.bg2.y - Globals.SCREEN_HEIGHT * 2

    def end_room(self):
        self.running = False
        self.background_sound.stop()

    def add_count(self):
        Globals.animals += 1
        self.animal_text.text = f"Collected {Globals.animals} of 5 Animals"
        self.animal_text.update_text()

        #if Globals.animals >= 4:
        #    self.animal_door.x = 2000
        #    self.delete_object(self.animal_door)

        #self.coin_text = TextObject(self, 680, 50, 'animals: %i' % Globals.coins, size=20)
        #self.coin_text.depth = 1000
        #self.coin_text.colour = (255, 255, 255)
        #self.coin_text.update_text()
        #self.add_room_object(self.animal_text)
