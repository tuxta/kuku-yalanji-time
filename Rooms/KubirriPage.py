from GameFrame import Level, Globals, EnumLevels
import os
from Objects import BackButton


class KubirriPage(Level):
    def __init__(self, screen, joysticks):
        Level.__init__(self, screen, joysticks)

        self.set_background_image(os.path.join('InfoPage', 'Kubirri.png'))

        self.back_button = BackButton(self, 500, 550, 20 * 30)
        self.add_room_object(self.back_button)

        narration = self.load_sound("kubirri.ogg")
        narration.play()
