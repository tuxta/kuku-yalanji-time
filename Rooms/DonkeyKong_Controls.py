from GameFrame import Level, Globals, EnumLevels
from Objects.NextButton import NextButton
import os


class DonkeyKong_Controls(Level):

    def __init__(self, screen, joysticks):
        Level.__init__(self, screen, joysticks)

        self.set_background_image(os.path.join('DK', 'Controls.png'))
        self.set_timer(6 * 30, self.add_next_button)

        self.instructions = self.load_sound(os.path.join('MD_Story', 'KubirriClimb.ogg'))
        self.instructions.play()

    def add_next_button(self):
        button = NextButton(self, Globals.SCREEN_WIDTH / 2 - 64, 340)
        self.add_room_object(button)

    def next_background(self):
        Globals.next_level = EnumLevels.DonkeyKong
        self.running = False

