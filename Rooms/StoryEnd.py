import os
from GameFrame import Level, Globals, EnumLevels
from Objects.NextButton import NextButton


class StoryEnd(Level):
    def __init__(self, screen, joysticks):
        Level.__init__(self, screen, joysticks)

        self.total_pages = 3
        self.curr_page = 0
        start_page = 9
        self.play_times = [16 * 30 - 10, 21 * 30, 11 * 30]
        self.narrations = []
        self.pages = []
        for i in range(start_page, start_page + self.total_pages):
            file_name = f"page_{i + 1}.png"
            background_image = os.path.join('MD_story', file_name)
            self.pages.append(background_image)
            audio_file_name = f"page_{i + 1}.ogg"
            audio_file = os.path.join('MD_Story', audio_file_name)
            self.narrations.append(audio_file)

        self.set_background_image(self.pages[self.curr_page])
        self.background_music = self.load_sound(os.path.join('MD_Story', 'story_music.ogg'))
        self.background_music.set_volume(0.1)
        self.background_music.play(-1)

        self.set_timer(self.play_times[self.curr_page], self.add_next_button)
        self.play_narration()

    def next_background(self):
        self.curr_page += 1
        if self.curr_page < self.total_pages:
            self.set_background_image(self.pages[self.curr_page])
            self.set_timer(self.play_times[self.curr_page], self.add_next_button)
            self.play_narration()
        else:
            self.background_music.stop()
            Globals.next_level = EnumLevels.TitleRoom
            self.running = False

    def play_narration(self):
        narration = self.load_sound(self.narrations[self.curr_page])
        narration.play()

    def add_next_button(self):
        button = NextButton(self, Globals.SCREEN_WIDTH / 2 - 64, 256)
        self.add_room_object(button)
