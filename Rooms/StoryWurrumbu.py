import os
from GameFrame import Level, Globals, EnumLevels
from Objects.NextButton import NextButton


class StoryWurrumbu(Level):
    def __init__(self, screen, joysticks):
        Level.__init__(self, screen, joysticks)

        self.set_background_image(os.path.join('MD_story', 'page_7.png'))
        self.background_music = self.load_sound(os.path.join('MD_Story', 'story_music.ogg'))
        self.background_music.set_volume(0.1)
        self.background_music.play(-1)

        narration = self.load_sound(os.path.join('MD_Story', 'page_7.ogg'))
        narration.play()
        self.set_timer(25 * 30, self.add_next_button)

    def next_background(self):
        self.background_music.stop()
        Globals.next_level = EnumLevels.FightRoom
        self.running = False

    def add_next_button(self):
        button = NextButton(self, Globals.SCREEN_WIDTH / 2 - 64, 256)
        self.add_room_object(button)
