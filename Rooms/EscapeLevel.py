from GameFrame import Level, Globals
from Objects import RunFloor, ScrollingBg, Obstacle, RunningPlayer, HealthHUD, Wurrumbu, ScoreText, FPSCounter
import os


class EscapeLevel(Level):
    def __init__(self, screen, joysticks):
        Level.__init__(self, screen, joysticks)

        if Globals.debug:
            self.fps_counter = FPSCounter(self, 0, 720 - 128)
            self.add_room_object(self.fps_counter)

        # load sounds
        self.jump_sound = self.load_sound(os.path.join("Escape", "jump.wav"))
        self.crash_sound = self.load_sound(os.path.join("Escape", "break.wav"))
        self.health_sound = self.load_sound(os.path.join("Escape", "health_pickup.wav"))

        # score
        self.dist_travelled = 0

        # used for increasing the speed
        self.stage_dist = 300
        self.dist_at_last_stage = 0
        self.speed_steepness = 1.1
        self.stage_steepness = 1.2

        self.move_speed = 20  # Controls the speeds of obstacles and the background
        self.set_background_image(os.path.join("Escape", "background.png.png"))
        self.set_background_scroll(20 / 20)

        # adds the floor
        self.floor = RunFloor(self, 0, 720 - 128)
        self.floor2 = RunFloor(self, 1280, 720 - 128)
        self.add_room_object(self.floor)
        self.add_room_object(self.floor2)

        # bg objects
        self.clouds = ScrollingBg(self, 0, -75, "bgClouds.png", 256, self.move_speed * 0.1, -200)
        self.clouds2 = ScrollingBg(self, 1280, -75, "bgClouds.png", 256, self.move_speed * 0.1, -200)
        self.add_room_object(self.clouds)
        self.add_room_object(self.clouds2)

        self.trees = ScrollingBg(self, 0, 720 - 128 - 512, "bgTrees.png", 512, self.move_speed * 0.35)
        self.trees2 = ScrollingBg(self, 1280, 720 - 128 - 512, "bgTrees.png", 512, self.move_speed * 0.35)
        self.add_room_object(self.trees)
        self.add_room_object(self.trees2)

        self.bushes = ScrollingBg(self, 0, 720 - 256, "bgBushes.png", 128, self.move_speed * 0.5)
        self.bushes2 = ScrollingBg(self, 1280, 720 - 256, "bgBushes.png", 128, self.move_speed * 0.5)
        self.bushes.depth = 1000
        self.bushes2.depth = 1000
        self.add_room_object(self.bushes)
        self.add_room_object(self.bushes2)

        # add obstacles
        self.obstacle = Obstacle(self, 1350, 720 - 128 - 64)
        self.obstacle.depth = 10000
        self.add_room_object(self.obstacle)

        # add player
        self.player = RunningPlayer(self, -64, 720 - 128 - 64)
        self.player.depth = 5000
        self.add_room_object(self.player)

        self.player_intro_speed = 7
        self.enemy_intro_speed = 4

        self.health_hud = HealthHUD(self, 10, 10)
        self.add_room_object(self.health_hud)

        self.game_over_cutscene_playing = False

        # add wurrumbu
        self.enemy = Wurrumbu(self, 0, 0)
        self.enemy.x = -self.enemy.width
        self.enemy.y = 720 - 128 - self.enemy.height - 10
        self.add_room_object(self.enemy)

        # add score text
        self.score_text = ScoreText(self, 0, 0)
        self.score_text.x = 1280 - self.score_text.width
        self.add_room_object(self.score_text)
        # adding a shadow to the text for no reason other than it looks cool
        self.score_text_shadow = ScoreText(self, self.score_text.x, self.score_text.y)
        self.score_text_shadow.size = 61
        self.score_text_shadow.colour = (100, 100, 100)
        self.score_text_shadow.depth = 29000
        self.add_room_object(self.score_text_shadow)

    def update(self):
        # intro sequence
        if self.player.x != 200 and not self.game_over_cutscene_playing:
            if self.player.x + self.player_intro_speed > 200:
                self.player.x + self.player.x - 200
            else:
                self.player.x += round(self.player_intro_speed)
            if self.player.x < 160:
                self.player_intro_speed += 0.15
            self.player.control_enabled = False
        elif self.enemy.x != 4:
            if self.enemy.x + self.enemy_intro_speed > 5:
                self.enemy.x + self.enemy.x - 5
            else:
                self.enemy.x += round(self.enemy_intro_speed)
            if self.enemy.x < -5:
                self.enemy_intro_speed += 0.1
            self.player.control_enabled = False
        else:
            self.player.control_enabled = True

        # start playing game over sequence when running out of lives
        if Globals.LIVES <= 0:
            self.player.control_enabled = False
            self.player.flashing = False
            self.game_over_cutscene_playing = True

        # move player into wurrumbu in game over sequence
        if self.game_over_cutscene_playing:
            self.player.x -= 5

        # end level when player dies
        if self.player.x <= self.enemy.x + self.enemy.width / 2 and self.game_over_cutscene_playing:
            self.running = False

        # update score and score text
        if self.player.control_enabled:
            self.dist_travelled += 1
            self.score_text.update_text_custom()
            self.score_text_shadow.update_text_custom()
            self.score_text.x = 1280 - self.score_text.width
            self.score_text_shadow.x = self.score_text.x - 5

        # calculate speed increases
        # totally didnt steal from another game i made lol
        if self.dist_travelled >= self.stage_dist + self.dist_at_last_stage:
            self.stage_dist *= self.stage_steepness
            self.dist_at_last_stage = self.dist_travelled
            self.move_speed *= self.speed_steepness
