import os
from GameFrame import Level
from Objects import ForagerMap, Wurrumbu, ForagingPlayer


class EscapeLevelIntro(Level):
    def __init__(self, screen, joysticks):
        Level.__init__(self, screen, joysticks)
        self.move_speed = 12

        background = ForagerMap(self, -310, -256)
        self.add_room_object(background)

        self.wurrumbu_cutscene = Wurrumbu(self, -200, 360)
        self.add_room_object(self.wurrumbu_cutscene)

        # player that can't be controlled
        self.forager_cutscene = ForagingPlayer(self, 0, 380)
        self.forager_cutscene.handle_key_events = False
        self.forager_cutscene.facing = 1
        self.forager_cutscene.animation_folder = "Foraging"
        self.forager_cutscene.animation = ["right_1.png", "right_2.png"]
        self.forager_cutscene.playing = True
        self.add_room_object(self.forager_cutscene)

        self.runaway = self.load_sound(os.path.join('MD_Story', 'RunFromWurrumbu.ogg'))
        self.runaway.play()

    def update(self):
        self.wurrumbu_cutscene.x += self.move_speed
        self.forager_cutscene.x += self.move_speed

        if self.wurrumbu_cutscene.x >= 1280 + 124:
            self.running = False
