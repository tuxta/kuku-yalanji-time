from GameFrame import Level, TextObject, Globals, EnumLevels
from Objects import Player, Bullet, HealthBar, Enemy, Spear, AmmoBox, EnemyFullBullet, EnemySplitBullet, DashEnemy, Enemy_Health_Bar, eHealthBarOutline
import random
import os


class FightRoom(Level):
    def __init__(self, screen, joysticks):
        Level.__init__(self, screen, joysticks)

        self.set_background_image(os.path.join('Fight', 'BG.png'))

        self.enemy_health = 50

        # - Create an instance of the Player object - #
        self.Player = Player(self, 200, 360)
        self.add_room_object(self.Player)

        # - Create a health bar to show the player's current health - #
        self.healthBar = HealthBar(self, 32, 32)
        self.add_room_object(self.healthBar)

        # - Create an instance of the Enemy object - #
        self.Enemy = Enemy(self, 800, 360)
        self.add_room_object(self.Enemy)

        self.Spear = Spear(self, self.Player.x, self.Player.y)
        self.add_room_object(self.Spear)

        self.AmmoText = TextObject(self, 900, 32, "Ammunition: " + str(self.Player.ammoCount))
        self.AmmoText.depth = 10
        self.AmmoText.colour = (255, 255, 255)
        self.AmmoText.size = 50
        self.AmmoText.font = 'Calibri'
        self.AmmoText.update_text()
        self.add_room_object(self.AmmoText)

        self.enemyHealthBar = Enemy_Health_Bar(self, 137, 652)
        self.add_room_object(self.enemyHealthBar)
        self.healthBarOutline = eHealthBarOutline(self, 90, 640)
        self.add_room_object(self.healthBarOutline)

        self.generateBlankBox()

        Globals.next_level = EnumLevels.StoryClimb

    # - Creates an instance of the bullet object at the player's position - #
    def shoot(self, playerDir):
        self.add_room_object(Bullet(self, self.Player.rect.centerx, self.Player.rect.centery, playerDir))

    def enemyShoot(self, bulletDir):
        self.add_room_object(EnemyFullBullet(self, self.Enemy.rect.centerx, self.Enemy.rect.centery, bulletDir))

    def splitEnemyBullet(self, enemyBulletX, enemyBulletY):
        self.add_room_object(EnemySplitBullet(self, enemyBulletX, enemyBulletY, 'left'))
        self.add_room_object(EnemySplitBullet(self, enemyBulletX, enemyBulletY, 'right'))
        self.add_room_object(EnemySplitBullet(self, enemyBulletX, enemyBulletY, 'up'))
        self.add_room_object(EnemySplitBullet(self, enemyBulletX, enemyBulletY, 'down'))
        self.add_room_object(EnemySplitBullet(self, enemyBulletX, enemyBulletY, 'leftup'))
        self.add_room_object(EnemySplitBullet(self, enemyBulletX, enemyBulletY, 'leftdown'))
        self.add_room_object(EnemySplitBullet(self, enemyBulletX, enemyBulletY, 'rightup'))
        self.add_room_object(EnemySplitBullet(self, enemyBulletX, enemyBulletY, 'rightdown'))

    def dashPhase(self, enemyX, enemyY):
        self.Enemy = DashEnemy(self, enemyX, enemyY)
        self.add_room_object(self.Enemy)

    def endGame(self):
        self.running = False

    def generateBlankBox(self):
        generateX = random.randint(42, 1196)
        generateY = random.randint(42, 646)
        self.add_room_object(AmmoBox(self, generateX, generateY))

        self.set_timer(300, self.generateBlankBox)
