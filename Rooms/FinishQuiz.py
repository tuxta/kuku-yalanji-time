import pygame
from GameFrame import Level
from Objects import Congratulations, BackButton
import os


class FinishQuiz(Level):
    def __init__(self, screen, joysticks):
        Level.__init__(self, screen, joysticks)

        self.set_background_image(os.path.join('InfoPage', 'Quiz.jpg'))

        self.congratulations = Congratulations(self, 265, 50)
        self.add_room_object(self.congratulations)

        self.back_button = BackButton(self, 500, 490, 30)
        self.add_room_object(self.back_button)

