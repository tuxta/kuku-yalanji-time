from GameFrame import Level, Globals
import os
from Objects import AboutButton, ManjalButton, WurrumbuButton, InformationPageButtons, KBButton, QuizButton, KubirriButton


class InfoPage(Level):
    def __init__(self, screen, joysticks):
        Level.__init__(self, screen, joysticks)

        self.set_background_image("dragonfly.jpg")

        kubirri_button = KubirriButton(self, 100, 425)
        kbbutton = KBButton(self, 900, 425)
        quiz_button = QuizButton(self, 500, 425)
        history_button = AboutButton(self, 100, 150)
        manjal_button = ManjalButton(self, 500, 150)
        wurrumbu_button = WurrumbuButton(self, 900, 150)
        information = InformationPageButtons(self, 400, 50, wurrumbu_button, history_button, manjal_button, kubirri_button, kbbutton, quiz_button)

        self.add_room_object(history_button)
        self.add_room_object(manjal_button)
        self.add_room_object(wurrumbu_button)
        self.add_room_object(information)
        self.add_room_object(kubirri_button)
        self.add_room_object(kbbutton)
        self.add_room_object(quiz_button)



