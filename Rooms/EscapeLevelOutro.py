from GameFrame import Level, RoomObject, Globals, EnumLevels
from Objects import ForagerMap, Player
import os


class EscapeLevelOutro(Level):
    def __init__(self, screen, joysticks):
        Level.__init__(self, screen, joysticks)
        self.move_speed = 12
        self.stop_pos = 640

        background = ForagerMap(self, -310, -256)
        self.add_room_object(background)

        wurrumbu = RoomObject(self, 780, 360)
        wurrumbu.set_image(wurrumbu.load_image(os.path.join("Escape", "WurrumbuCutscene.png")), 31 * 6, 20 * 6)
        self.add_room_object(wurrumbu)

        self.player = Player(self, -100, 380)
        self.player.handle_key_events = False
        self.player.standing = False
        self.player.depth = 1000000
        self.add_room_object(self.player)

        self.timer = 0

    def update(self):
        if self.player.x < 640:
            self.player.x += self.move_speed
        else:
            self.player.standing = True
            self.timer += 1

        if self.timer >= 45:
            Globals.next_level = EnumLevels.StoryWurrumbu
            self.running = False
