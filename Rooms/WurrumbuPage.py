from GameFrame import Level, Globals, EnumLevels
from Objects import BackButton
import os


class WurrumbuPage(Level):
    def __init__(self, screen, joysticks):
        Level.__init__(self, screen, joysticks)

        self.set_background_image(os.path.join('InfoPage', 'Wurrumbu.png'))

        self.back_button = BackButton(self, 500, 550, 17 * 30)
        self.add_room_object(self.back_button)

        narration = self.load_sound("wurrumbu.ogg")
        narration.play()
