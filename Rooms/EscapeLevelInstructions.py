from GameFrame import Level, Globals
from Objects import Instructions
import os



class EscapeLevelInstructions(Level):
    def __init__(self, screen, joysticks):
        Level.__init__(self, screen, joysticks)

        self.instructions = Instructions(self, 0, 0)
        self.add_room_object(self.instructions)

        self.menu_sound = self.load_sound(os.path.join("Escape", 'controls.wav'))

        self.escape_instructions = self.load_sound(os.path.join('MD_Story', 'EscapeInstructions.ogg'))
        self.escape_instructions.play()
