from GameFrame import Level
from Objects import Next
import os


class Controls(Level):
    def __init__(self, screen, joysticks):
        Level.__init__(self, screen, joysticks)

        self.set_background_image(os.path.join('InfoPage', 'Controls.png'))

        self.next = Next(self, 455, 500)
        self.add_room_object(self.next)

