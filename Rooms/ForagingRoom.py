from GameFrame import Level, Globals, EnumLevels
import random
import os
from Objects import ForagingPlayer, Tree, LargeTree, Block, BlockBottom, River, Collectible, ForagerMap


class ForagingRoom(Level):

    def __init__(self, screen, joysticks):
        Level.__init__(self, screen, joysticks)

        Globals.icon_bar_length = 40

        self.room_items = []
        background = ForagerMap(self, -310, -256)
        self.add_room_object(background)

        # Load Sounds
        self.background_sound = self.load_sound("Background.wav")
        self.collectible_sound = self.load_sound("Collectible.wav")

        self.background_sound = self.load_sound('Background.wav')
        self.background_sound.set_volume(0.05)
        self.background_sound.play(-1)

        self.instructions = self.load_sound(os.path.join("MD_Story", "ExploreInstructions.ogg"))
        self.instructions.play()

        # - Set up maze, objects 32x32 - #
        room_objects = [
            'b  b  b  b  b  b  b  b  b  b  b  b  b  b  b  bR b  b  b  b  b  b  b  b  b  b  b  b  b  b  b  b  b',
            '                                                                                                 ',
            '                                                                                                 ',
            '                                                                                                 ',
            'b                                                                                               b',
            '                                                                                                 ',
            '                                     t                                                           ',
            '                                                               t                                 ',
            'b                                                                                               b',
            '                                                                                                 ',
            '                                                                                                 ',
            '                        T                                                                        ',
            'b  t                                                                                            b',
            '                                                                                                 ',
            '                                                                                                 ',
            '                P                                                                                ',
            'b                                                                                               b',
            '                                                                                                 ',
            '                                                                          t                      ',
            '                                                                                                 ',
            'b                                                                                               b',
            '                                                                                                 ',
            '                                             T                                                   ',
            '                                                                                                 ',
            'b                                                                                               b',
            '                                                                                                 ',
            '                                                                                                 ',
            '                                                                                                 ',
            'b                                                                                               b',
            '                                                                                                 ',
            '                                                                                                 ',
            '                                                                                                 ',
            'b                                                                                               b',
            '                                                                                                 ',
            '                      T                                                                          ',
            '                                                                                                 ',
            'b                                                                                               b',
            '                                                                                                 ',
            '                                                                          T                      ',
            '                                                                                                 ',
            'b                                                                                               b',
            '                                                                                                 ',
            '                                                                                                 ',
            '                                                                                                 ',
            'b                          t                                                                    b',
            '                                                                                                 ',
            '                                                                                                 ',
            '                                                                                                 ',
            'B  B  B  B  B  B  B  B  B  B  B  B  B  B  B  B  B  B  B  B  B  B  B  B  B  B  B  B  B  B  B  B  B'

        ]

        new_river = River(self, 0, 0)

        for i, row in enumerate(room_objects):
            for j, obj in enumerate(row):
                if obj == 'P':
                    self.player = ForagingPlayer(self, j * 32 - 200, i * 32 - 200)
                    self.add_room_object(self.player)
                elif obj == 'b':
                    new_block = Block(self, j * 32 - 200, i * 32 - 200)
                    self.add_room_object(new_block)
                    self.room_items.append(new_block)
                elif obj == 'B':
                    new_blockBottom = BlockBottom(self, j * 32 - 200, i * 32 - 200)
                    self.add_room_object(new_blockBottom)
                    self.room_items.append(new_blockBottom)
                elif obj == 'T':
                    new_tree = Tree(self, j * 32 - 200, i * 32 - 200)
                    self.add_room_object(new_tree)
                    self.room_items.append(new_tree)
                elif obj == 't':
                    new_largeTree = LargeTree(self, j * 32 - 200, i * 32 - 200)
                    self.add_room_object(new_largeTree)
                    self.room_items.append(new_largeTree)
                elif obj == 'R':
                    new_river.x = j * 32 - 200
                    new_river.y = i * 32 - 200
                    self.add_room_object(new_river)

        flora_fauna = list()
        flora_fauna.append(Collectible(self, 0, 0, 'turkey'))
        flora_fauna.append(Collectible(self, 0, 34, 'cassowary'))
        flora_fauna.append(Collectible(self, 0, 68, 'crocodile'))
        flora_fauna.append(Collectible(self, 0, 102, 'mullet'))
        flora_fauna.append(Collectible(self, 0, 136, 'drop vine'))
        flora_fauna.append(Collectible(self, 0, 170, 'quandong'))
        flora_fauna.append(Collectible(self, 0, 205, 'plum'))
        flora_fauna.append(Collectible(self, 0, 239, 'banana'))
        flora_fauna.append(Collectible(self, 0, 273, 'beech'))
        flora_fauna.append(Collectible(self, 0, 307, 'gunya'))

        for item in flora_fauna:
            unique_spot = False
            while not unique_spot:
                item.x = random.randint(64, 30 * 3 * 32)
                item.y = random.randint(64, 10 * 4 * 32)
                # check x and y against all other objects
                # if none in same area, set unique_spot to True
                collision_found = False
                for other in self.room_items:
                    if other is not item:
                        other_type = type(other).__name__
                        if other_type != 'Tree' and other_type != 'LargeTree':
                            if abs(other.x - item.x) < 32 and abs(other.y - item.y) < 32: # other.rect.colliderect(item.rect):
                                collision_found = True
                                break
                        else:
                            if item.y < other.y + 250 and \
                                    item.y + 32 > other.y and \
                                    item.x < other.x + 202 and \
                                    item.x + 32 > other.x:
                                collision_found = True
                                break
                if not collision_found:
                    unique_spot = True

            self.add_room_object(item)
            self.room_items.append(item)

        self.room_items.append(new_river)
        self.room_items.append(background)

    def shift_room_left(self):
        for item in self.room_items:
            item.x -= Globals.move_speed

    def shift_room_right(self):
        for item in self.room_items:
            item.x += Globals.move_speed

    def shift_room_up(self):
        for item in self.room_items:
            item.y -= Globals.move_speed

    def shift_room_down(self):
        for item in self.room_items:
            item.y += Globals.move_speed

    def end_room(self):
        Globals.next_level = EnumLevels.StoryMid
        self.running = False
        self.background_sound.stop()
