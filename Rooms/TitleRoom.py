from GameFrame import Level, Globals
from Objects import TitlePlayButton, TitleInfoButton, KukuLalanjiTitleText


class TitleRoom(Level):
    def __init__(self, screen, joysticks):
        Level.__init__(self, screen, joysticks)

        self.set_background_image("title_background.png")

        info_button = TitleInfoButton(self, 0, 0)
        info_button.x = Globals.SCREEN_WIDTH - info_button.width
        play_button = TitlePlayButton(self, 0, 0)
        title_text = KukuLalanjiTitleText(self, 0, 0, play_button, info_button)
        title_text.x = Globals.SCREEN_WIDTH - title_text.width
        title_text.y = Globals.SCREEN_HEIGHT - title_text.height

        self.add_room_object(play_button)
        self.add_room_object(info_button)
        self.add_room_object(title_text)
