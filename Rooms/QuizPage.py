from GameFrame import Level
from Objects import No, Yes, QuestionNumber
import os


class QuizPage(Level):
    def __init__(self, screen, joysticks):
        Level.__init__(self, screen, joysticks)

        self.no = No(self, 400, 500)
        self.yes = Yes(self, 700, 500)
        self.question_number = QuestionNumber(self, 265, 50, self.yes, self.no)

        self.add_room_object(self.no)
        self.add_room_object(self.yes)
        self.add_room_object(self.question_number)

        self.current_question = 0

        self.set_background_image(os.path.join('InfoPage', 'Quiz.jpg'))
