from GameFrame import Level
from Objects import LevelSelectText


class LevelSelect(Level):
    def __init__(self, screen, joysticks):
        Level.__init__(self, screen, joysticks)
        self.text = LevelSelectText(self, 400, 280)
        self.add_room_object(self.text)