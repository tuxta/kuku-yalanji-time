from GameFrame import RoomObject, Globals, EnumLevels
import os


class AboutButton(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        self.selected_image = self.load_image(os.path.join('InfoPage', 'AboutHighlight.png'))
        self.unselected_image = self.load_image(os.path.join('InfoPage', 'AboutButton.png'))

        self.set_image(self.unselected_image, 300, 200)

    def set_selected(self, selected):
        if selected:
            self.set_image(self.selected_image, 300, 200)
        else:
            self.set_image(self.unselected_image, 300, 200)





