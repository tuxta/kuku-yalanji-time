from GameFrame import RoomObject
import os


class AmmoBox(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        self.width = 40
        self.height = 40

        self.image = self.load_image(os.path.join('Fight', 'Crate Spear 2.png'))
        self.set_image(self.image, self.width, self.height)

        self.register_collision_object('Player')

    def handle_collision(self, other, other_type):
        if other_type == 'Player':
            self.set_timer(1, self.deleteSelf)

    def deleteSelf(self):
        self.delete_object(self)
