from GameFrame import RoomObject, Globals


class CollectibleIconDK(RoomObject):
    def __init__(self, room, x, y, image):
        RoomObject.__init__(self, room, x, y)

        image_width = 32
        image_length = 32
        self.depth = 100

        img = self.load_image(image)
        self.set_image(img, image_width, image_length)

        self.room.add_count()
