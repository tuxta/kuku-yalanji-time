from GameFrame import RoomObject
import random
import os


class Obstacle(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        self.starting_x = self.x
        self.depth = 250000
        self.animation_folder = "Escape"

        # list of starting obstacles
        self.obstacles = ['Rock', 'Log']
        self.obstacle = "Rock"

        self.resetting = False

        # load obstacle images
        self.rock_img = self.load_image(os.path.join("Escape", "Rock.png"))
        self.log_img = self.load_image(os.path.join("Escape", "Log.png"))
        self.medium_rock = self.load_image(os.path.join("Escape", "MediumRock.png"))
        self.medium_log = self.load_image(os.path.join("Escape", "MediumLog.png"))
        self.crouch_log = self.load_image(os.path.join("Escape", "CrouchLog.png"))
        self.large_log = self.load_image(os.path.join("Escape", "LargeLog.png"))
        self.large_rock = self.load_image(os.path.join("Escape", "LargeRock.png"))

        self.heart_anim = ["Heart1.png",
                           "Heart2.png"]

        self.width = 64
        self.height = 64
        self.set_image(self.rock_img, self.width, self.height)
        self.is_heart = False
        self.animation_interval = 5
        self.heart_chance = 5  # percent chance of spawning a heart

    def switch_obstacle(self):
        # random chance to turn into a heart instead of an obstacle
        if random.randint(0, 100) <= self.heart_chance:
            self.width = 64
            self.height = 64
            self.animation = self.heart_anim
            self.set_image(self.load_image(os.path.join("Escape", "Heart1.png")), self.width, self.height)
            self.playing = True
            self.is_heart = True
            self.visible = True
            return
        else:
            self.animation = []
            self.playing = False
            self.is_heart = False

        # pick a random obstacle out of the list
        self.obstacle = self.obstacles[random.randint(0, len(self.obstacles) - 1)]

        # adjust properties accordingly
        if self.obstacle == 'Rock':
            self.width = 64
            self.height = 64
            self.set_image(self.rock_img, self.width, self.height)
        elif self.obstacle == 'Log':
            self.width = 64
            self.height = 64
            self.set_image(self.log_img, self.width, self.height)
        elif self.obstacle == 'Medium Log':
            self.width = 64
            self.height = 96
            self.set_image(self.medium_log, self.width, self.height)
        elif self.obstacle == "Medium Rock":
            self.width = 64
            self.height = 96
            self.set_image(self.medium_rock, self.width, self.height)
        elif self.obstacle == "Crouch Log":
            self.width = 64
            self.height = 564
            self.set_image(self.crouch_log, self.width, self.height)
        elif self.obstacle == "Large Log":
            self.width = 64
            self.height = 256
            self.set_image(self.large_log, self.width, self.height)
        elif self.obstacle == "Large Rock":
            self.width = 64
            self.height = 192
            self.set_image(self.large_rock, self.width, self.height)

        self.visible = True

    # epic utility function to check if an obstacle has been added yet :)
    def check_for_obstacle(self, name):
        for i in self.obstacles:
            if i == name:
                return True
        return False

    def step(self):

        if self.resetting:
            if self.is_heart:
                self.y = 592 - self.height - 100
            elif self.obstacle == "Crouch Log":
                self.y = 0
            else:
                self.y = 592 - self.height
            self.x = 1280
            self.resetting = False

        if self.x < 1280 and self.animation == []:
            if self.is_heart:
                self.x = 1280

        # adjust obstacle list as the player progresses
        if not self.check_for_obstacle('Medium Log'):
            if self.room.dist_travelled >= 400:
                self.obstacles.append('Medium Log')

        if not self.check_for_obstacle('Medium Rock'):
            if self.room.dist_travelled >= 800:
                self.obstacles.append('Medium Rock')

        if not self.check_for_obstacle('Crouch Log'):
            if self.room.dist_travelled >= 1500:
                self.obstacles.append('Crouch Log')

        if not self.check_for_obstacle('Large Rock'):
            if self.room.dist_travelled >= 2000:
                self.obstacles.append('Large Rock')

        if not self.check_for_obstacle('Large Log'):
            if self.room.dist_travelled >= 3600:
                self.obstacles.append('Large Log')

        if self.check_for_obstacle('Log'):
            if self.room.dist_travelled >= 1300:
                self.obstacles.remove('Log')

        if self.check_for_obstacle('Rock'):
            if self.room.dist_travelled >= 1500:
                self.obstacles.remove('Rock')

        if self.check_for_obstacle('Medium Log'):
            if self.room.dist_travelled >= 4200:
                self.obstacles.remove('Medium Log')

        # loops obstacle
        if self.room.player.control_enabled:
            if self.x > 0 - self.width:
                self.x -= round(self.room.move_speed)
            else:
                # switch obstacles and reset
                self.switch_obstacle()
                self.resetting = True


