from GameFrame import RoomObject, Globals, EnumLevels
import pygame


class KukuLalanjiTitleText(RoomObject):
    def __init__(self, room, x, y, play_button, info_button):
        RoomObject.__init__(self, room, x, y)

        self.play_button = play_button
        self.info_button = info_button

        self.selected_button = "play"

        image = self.load_image("KukuYalanji_title.png")
        self.set_image(image, 951, 264)

        self.handle_key_events = True

        self.forest_sounds = self.room.load_sound("daintree_rainforrest_birds.ogg")
        self.forest_sounds.set_volume(0.2)
        self.forest_sounds.play(-1)

    def right_press(self):
        self.selected_button = "info"
        self.play_button.set_selected(False)
        self.info_button.set_selected(True)

    def left_press(self):
        self.selected_button = "play"
        self.play_button.set_selected(True)
        self.info_button.set_selected(False)

    def select_press(self):
        if self.selected_button == "play":
            Globals.next_level = EnumLevels.StoryIntro
            self.forest_sounds.stop()
            pygame.mixer.stop()
        else:
            Globals.next_level = EnumLevels.Controls

        self.room.running = False

    def key_pressed(self, key):
        if key[pygame.K_RIGHT]:
            self.right_press()
        elif key[pygame.K_LEFT]:
            self.left_press()
        elif key[pygame.K_RETURN]:
            self.select_press()

    def joy_pad_signal(self, p1_buttons, p2_buttons):
        if p1_buttons[11] > 0.5:
            self.right_press()
        elif p1_buttons[11] < -0.5:
            self.left_press()
        elif p1_buttons[1]:
            self.select_press()
