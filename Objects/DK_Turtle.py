from GameFrame import RoomObject
import os


class Turtle(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        image = self.load_image(os.path.join('DK','Turtle.png'))
        self.set_image(image, 48, 48)
