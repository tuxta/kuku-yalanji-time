from GameFrame import RoomObject
import os


class Block(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        image = self.load_image(os.path.join('Foraging', 'block.png'))
        self.set_image(image, 95, 139)
