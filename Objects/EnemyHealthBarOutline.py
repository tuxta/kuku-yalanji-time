from GameFrame import RoomObject
import os


class eHealthBarOutline(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        self.width = 1100
        self.height = 42

        self.image1 = self.load_image(os.path.join('Fight', 'HealthBarOutline1.png'))
        self.image2 = self.load_image(os.path.join('Fight', 'HealthBarOutline2.png'))
        self.set_image(self.image1, self.width, self.height)

    def changeImagePhase(self):
        pass
        # self.set_image(self.image2, self.width, self.height)

    def step(self):
        if self.room.enemy_health <= 25:
            self.set_timer(1, self.changeImagePhase)
