from GameFrame import TextObject, Globals, EnumLevels
import pygame


class LevelSelectText(TextObject):
    def __init__(self, room, x, y):
        TextObject.__init__(self, room, x, y)
        self.size = 80
        self.colour = (255, 255, 255)
        self.handle_key_events = True
        self.key_cooldown = 0.5
        self.on_cooldown = False
        self.timer = 0

        self.current_level = 0
        self.text = Globals.levels[self.current_level]
        self.update_text_custom()

    # function for looping through the levels
    def progress(self, direction):
        if direction == "forward":
            if self.current_level + 1 >= len(Globals.levels):
                self.current_level = 0
            else:
                self.current_level += 1
                # i have to do this so that you cant select this room from this room if you know what i mean
                if self.current_level == EnumLevels.LevelSelect:
                    self.current_level = 0

        if direction == "backwards":
            if self.current_level - 1 < 0:
                self.current_level = len(Globals.levels) - 2
            else:
                self.current_level -= 1
                if self.current_level == EnumLevels.LevelSelect:
                    self.current_level = len(Globals.levels) - 1

        self.text = Globals.levels[self.current_level]
        self.update_text_custom()

    def key_pressed(self, key):
        # progress through the levels
        if key[pygame.K_RIGHT]:
            if not self.on_cooldown:
                self.progress("forward")
                self.on_cooldown = True
        if key[pygame.K_LEFT]:
            if not self.on_cooldown:
                self.progress("backwards")
                self.on_cooldown = True

        if key[pygame.K_z]:
                # switch to selected room
                exec("Globals.next_level = EnumLevels." + Globals.levels[self.current_level])
                self.room.running = False

    def joy_pad_signal(self, p1_buttons, p2_buttons):
        if p1_buttons[11] > 0.5:
            if not self.on_cooldown:
                self.progress("forward")
                self.on_cooldown = True

        if p1_buttons[11] < -0.5:
            if not self.on_cooldown:
                self.progress("backwards")
                self.on_cooldown = True

        if p1_buttons[1] or p1_buttons[2]:
            exec("Globals.next_level = EnumLevels." + Globals.levels[self.current_level])
            self.room.running = False

    def step(self):
        # cooldown so it doesn't go really fast when you hold a button
        if self.on_cooldown:
            self.timer += 0.0333333
            if self.timer >= self.key_cooldown:
                self.on_cooldown = False
                self.timer = 0

    # update da text
    def update_text_custom(self):
        self.built_font = pygame.font.Font('Objects/VCR_OSD_MONO.ttf', self.size)
        self.rendered_text = self.built_font.render(self.text, False, self.colour)
        self.image = self.rendered_text
        self.width, self.height = self.built_font.size(self.text)
        self.rect = pygame.Rect(self.x, self.y, self.width, self.height)