from GameFrame import RoomObject
import os

class ScrollingBg(RoomObject):
    def __init__(self, x, y, room, img, height, speed, depth=0):
        RoomObject.__init__(self, x, y, room)

        self.speed = speed
        self.starting_x = self.x

        self.width = 1280
        self.height = height
        self.depth = depth

        self.img = self.load_image(os.path.join("Escape", img))
        self.set_image(self.img, self.width, self.height)

    def step(self):

        # moves object back like the floor
        if self.x > self.starting_x - 1280:
            self.x -= self.speed
        else:
            self.x = self.starting_x