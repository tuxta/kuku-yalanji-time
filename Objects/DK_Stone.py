from GameFrame import RoomObject
import os


class DK_Stone(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        image = self.load_image(os.path.join('DK', 'Stone.png'))
        self.set_image(image, 112, 112)

