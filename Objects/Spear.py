from GameFrame import RoomObject
import math, pygame, os


class Spear(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        self.set_image((self.load_image(os.path.join('Fight', 'Spear Sprite.png'))), 24, 62)
        self.depth = 2

        # Set the speed once in the constructor - it running every tick is useless
        self.speed = 8
        self.melee_active = False
        self.cooldown_active = False
        self.active_duration = 12
        self.half_melee = False
        self.cooldown_duration = 20
        self.position_offset = pygame.Vector2(0, 0)

    def rotateToEnemy(self, enemyX, enemyY):
        distanceX = self.x + (self.width / 2) - enemyX
        distanceY = self.y + (self.height / 2) - enemyY

        angle = math.degrees(math.atan2(distanceX, distanceY))

        self.curr_rotation = 0
        self.rotate(int(angle))

    def step(self):
        # always move onto the player, even while attacking
        self.x = self.room.Player.x + self.width
        self.y = self.room.Player.y + self.height / 2

        # Merge the two if statements into one set (mainly for readability)
        if self.melee_active:
            if self.half_melee:
                # Every tick with melee active, accumulate the spear's offset from the player, then apply it
                self.position_offset += self.melee_direction.normalize() * self.speed

            else:
                self.position_offset -= self.melee_direction.normalize() * self.speed

            self.x += self.position_offset.x
            self.y += self.position_offset.y
        else:
            self.rotateToEnemy(self.room.Enemy.rect.centerx, self.room.Enemy.rect.centery)

            # This also needs to be down here for some reason. Seems like a gameframe bug or smth
            self.x = self.room.Player.x + self.width
            self.y = self.room.Player.y + self.height / 2

    def melee(self):
        if not self.cooldown_active and not self.melee_active:
            self.melee_active = True
            self.half_melee = True
            self.melee_direction = pygame.Vector2(
                self.room.Enemy.x - self.x,
                self.room.Enemy.y - self.y
            )

            self.set_timer(self.active_duration, self.finish_melee)
            self.set_timer(self.active_duration / 2, self.finish_half_melee)

            self.room.Player.invulnerable = True
            self.set_timer(self.active_duration + self.active_duration / 2, self.no_damage)

    def no_damage(self):
        self.room.Player.invulnerable = False

    def finish_half_melee(self):
        self.half_melee = False

    def finish_melee(self):
        self.melee_active = False
        self.cooldown_active = True
        self.position_offset = pygame.Vector2(0, 0)

        self.set_timer(self.cooldown_duration, self.meleeCooldown)

    def meleeCooldown(self):
        self.cooldown_active = False
