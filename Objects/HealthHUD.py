from GameFrame import RoomObject, Globals
import os

class HealthHUD(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)
        self.full = self.load_image(os.path.join("Escape", "Full Health.png"))
        self.twohp = self.load_image(os.path.join("Escape", "Missing One Health.png"))
        self.onehp = self.load_image(os.path.join("Escape", "Missing Two Health.png"))
        self.nohp = self.load_image(os.path.join("Escape", "Missing All Health.png"))
        self.depth = 15000
        self.scale = 2
        self.width = 50 * self.scale
        self.height = 16 * self.scale

        # update based on how much health left
        if Globals.LIVES == 3:
            self.set_image(self.full, self.width, self.height)
        elif Globals.LIVES == 2:
            self.set_image(self.twohp, self.width, self.height)
        elif Globals.LIVES == 1:
            self.set_image(self.onehp, self.width, self.height)
        else:
            self.set_image(self.nohp, self.width, self.height)

    def step(self):
        if Globals.LIVES == 3:
            self.set_image(self.full, self.width, self.height)
        elif Globals.LIVES == 2:
            self.set_image(self.twohp, self.width, self.height)
        elif Globals.LIVES == 1:
            self.set_image(self.onehp, self.width, self.height)
        else:
            self.set_image(self.nohp, self.width, self.height)