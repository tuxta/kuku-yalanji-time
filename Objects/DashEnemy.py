import math
import pygame, os
from GameFrame import RoomObject, Globals


class DashEnemy(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)
        self.handle_key_events = True

        self.width = 124
        self.height = 80
        self.max_x = Globals.SCREEN_WIDTH - self.width
        self.max_y = Globals.SCREEN_HEIGHT - self.height
        self.speed = 4
        self.dashing = False  # possibly remove this variable
        self.dash_length = 35
        self.slowdown_length = 5
        self.dash_cooldown_length = 12
        self.stunned = False
        self.stun_length = 60
        self.dash_progress = 0 # possibly remove the need to initialise this

        self.dash_direction = pygame.Vector2
        self.on_dash_cooldown = False

        self.damage_cooldown = False

        self.healthy = self.load_image(os.path.join('Fight', 'Boss Sprite Left 1.png'))
        self.damaged = self.load_image(os.path.join('Fight', 'Boss Sprite Left 2.png'))

        self.set_image(self.healthy, self.width, self.height)

        # - Registers collision objects - #
        self.register_collision_object('Bullet')
        self.register_collision_object('Player')
        self.register_collision_object('Spear')

    def step(self):
        # Continue to move if mid-dash, otherwise begin another dash after the cooldown.
        if self.dashing:
            self.move_in_direction(self.dash_direction)
            self.dash_progress += 1
        elif not self.stunned and not self.dash_progress:
            self.set_timer(self.dash_cooldown_length, self.initialise_dash)
            self.dash_progress = 1

        if self.x < 0 or self.y < 0 or self.x > self.max_x or self.y > self.max_y:
            self.dashing = False
            self.stunned = True
            self.takeDamage()
            self.clamp_position()
            self.set_timer(self.stun_length, self.toggle_stunned)

    def move_in_direction(self, direction):
        # Add some sense of momentum to the end of the dash.
        velocity = math.sqrt(abs(self.dash_length - self.dash_progress)) * self.speed

        # NOTE: This breaks when the direction vector is (0, 0).
        motion = direction.normalize() * velocity

        # Move towards the target position at the calculated speed.
        self.x += motion.x
        self.y += motion.y

    def initialise_dash(self):
        # Store the target dash direction (towards the player).
        self.dash_direction = pygame.Vector2(self.room.Player.x - self.x, self.room.Player.y - self.y)

        # Begin dashing.
        self.set_timer(self.dash_length, self.end_dash) # possibly remove this timer
        self.dashing = True

    def end_dash(self):
        self.dashing = False
        self.dash_progress = 0

    def takeDamage(self):
        if not self.damage_cooldown:
            self.room.enemy_health -= 4
            self.damage_cooldown = True
            self.set_timer(20, self.reset_damage)
            if self.room.enemy_health <= 0:
                self.set_timer(1, self.killSelf)
            self.set_image(self.damaged, self.width, self.height)
            self.set_timer(5, self.resetImage)

    def reset_damage(self):
        self.damage_cooldown = False

    def resetImage(self):
        self.set_image(self.healthy, self.width, self.height)

    def killSelf(self):
        self.room.running = False

    def handle_collision(self, other, other_type):
        if other_type == 'Bullet':
            self.takeDamage()
        if other_type == 'Spear' and self.room.Spear.melee_active:
            self.takeDamage()
        elif other_type == 'Player' and not self.room.Player.dodge_active:
            self.on_dash_cooldown = True
            self.room.Player.takeDamage()
            self.takeDamage()

    def clamp_position(self):
        self.x = max(min(self.max_x, self.x), 0)
        self.y = max(min(self.max_y, self.y), 0)

    def toggle_stunned(self):
        self.stunned = not self.stunned
