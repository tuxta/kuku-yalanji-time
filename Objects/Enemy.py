from GameFrame import RoomObject
import random
import math
import os


class Enemy(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        self.width = 124
        self.height = 80

        self.curr_index = 1

        self.sprite_cycle = [
            self.load_image(os.path.join('Fight', 'Boss Sprite Left 1.png')),
            self.load_image(os.path.join('Fight', 'Boss Sprite Left 2.png'))
        ]

        self.set_image(self.sprite_cycle[self.curr_index], self.width, self.height)

        self.bulletDirs = ['left', 'right', 'up', 'down']

        self.damage_cooldown = False

        # - Registers collision objects - #
        self.register_collision_object('Bullet')
        self.register_collision_object('Player')
        self.register_collision_object('Spear')

        self.set_timer(30, self.shoot)

        self.velocity = 5

        self.randomX = self.x
        self.randomY = self.y
        self.atRandomPos = True

        self.randomMovePos()
        self.animate()

    def animate(self):
        self.set_image(self.sprite_cycle[self.curr_index], self.width, self.height)
        self.curr_index += 1
        self.curr_index %= 2
        self.set_timer(5, self.animate)

    def takeDamage(self):

        if not self.damage_cooldown:
            self.room.enemy_health -= 4
            self.damage_cooldown = True
            self.set_timer(20, self.reset_damage)

        if self.room.enemy_health <= 25:
            self.set_timer(1, self.changeToDashPhase)

    def reset_damage(self):
        self.damage_cooldown = False

    def shoot(self):
        self.room.enemyShoot(self.bulletDirs[random.randint(0, 3)])
        self.set_timer(100, self.shoot)

    def changeToDashPhase(self):
        self.room.dashPhase(self.x, self.y)
        self.delete_object(self)

    def randomMovePos(self):
        if self.atRandomPos:
            self.randomX = random.randint(280, 1000)
            self.randomY = random.randint(120, 600)
            self.atRandomPos = False

    def moveTowardsPos(self, randomX, randomY):
        distanceX = randomX - self.rect.centerx
        distanceY = randomY - self.rect.centery

        angle = math.atan2(distanceY, distanceX)

        speedX = self.velocity * math.cos(angle)
        speedY = self.velocity * math.sin(angle)

        self.x += speedX
        self.y += speedY

        if distanceX < 5 and distanceY < 5:
            self.atRandomPos = True

    def handle_collision(self, other, other_type):
        if other_type == 'Bullet':
            self.takeDamage()
        if other_type == 'Spear' and self.room.Spear.melee_active:
            self.takeDamage()
        if other_type == 'Player' and not self.room.Player.dodge_active:
            self.takeDamage()

    def step(self):
        self.randomMovePos()
        self.moveTowardsPos(self.randomX, self.randomY)