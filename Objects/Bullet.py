from GameFrame import RoomObject
import math, os


class Bullet(RoomObject):
    def __init__(self, room, x, y, direction):
        RoomObject.__init__(self, room, x, y)

        # - The width and height have been set here for later reference in the room - #
        self.width = 24
        self.height = 62
        self.depth = 2

        self.imageCount = 0
        self.image = self.load_image(os.path.join('Fight', 'Spear Sprite.png'))
        self.set_image(self.image, self.width, self.height)

        self.velocity = 15

        # - These values are set here for later reference in the object - #
        self.direction = direction

        # - Registers collision objects - #
        self.register_collision_object('Enemy')
        self.register_collision_object('DashEnemy')

    def moveToEnemy(self):
        distanceX = self.room.Enemy.rect.centerx - self.rect.centerx
        distanceY = self.room.Enemy.rect.centery - self.rect.centery

        angle = math.atan2(distanceY, distanceX)

        speedX = self.velocity * math.cos(angle)
        speedY = self.velocity * math.sin(angle)

        self.x += speedX
        self.y += speedY

    # - This function is called every frame, and is used here similarly to a while loop - #
    # - In the function, the bullet is moved across the screen in the given direction - #
    def rotateToEnemy(self, enemyX, enemyY):
        distanceX = self.x + (self.width / 2) - enemyX
        distanceY = self.y + (self.height / 2) - enemyY

        angle = math.degrees(math.atan2(distanceX, distanceY))

        self.curr_rotation = 0
        self.rotate(int(angle))

    def step(self):
        self.rotateToEnemy(self.room.Enemy.rect.centerx, self.room.Enemy.rect.centery)
        self.moveToEnemy()

        # - If the bullet is off the screen, delete the object - #
        if self.x < 0 or self.x > 1280:
            self.delete_object(self)
        elif self.y < 0 or self.y > 720:
            self.delete_object(self)

    def handle_collision(self, other, other_type):
        if other_type == 'Enemy' or other_type == 'DashEnemy':
            self.delete_object(self)
