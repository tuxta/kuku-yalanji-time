from GameFrame import RoomObject, Globals, EnumLevels
import pygame
import os


class QuestionNumber(RoomObject):
    def __init__(self, room, x, y, yes, no):
        RoomObject.__init__(self, room, x, y)

        self.yes = yes
        self.no = no

        self.can_select = True

        self.selected_button = "yes"

        self.handle_key_events = True

        self.current_question = 0

        # Images are not in order on purpose
        self.questions = [
            os.path.join('InfoPage', 'QuestionOne.png'),
            os.path.join('InfoPage', 'QuestionTwo.png'),
            os.path.join('InfoPage', 'QuestionSeven.png'),
            os.path.join('InfoPage', 'QuestionThree.png'),
            os.path.join('InfoPage', 'QuestionNine.png'),
            os.path.join('InfoPage', 'QuestionEight.png'),
            os.path.join('InfoPage', 'QuestionFour.png'),
            os.path.join('InfoPage', 'QuestionTen.png'),
            os.path.join('InfoPage', 'QuestionSix.png'),
            os.path.join('InfoPage', 'QuestionFive.png')
        ]
        image = self.load_image(self.questions[self.current_question])
        self.set_image(image, 900, 450)

        self.correct_popup = self.load_image(os.path.join('InfoPage', 'Correct.png'))
        self.incorrect_popup = self.load_image(os.path.join('InfoPage', 'Incorrect.png'))

        self.answers = [
            True,
            False,
            False,
            True,
            True,
            False,
            True,
            False,
            True,
            True
        ]

    def left_press(self):
        self.selected_button = "no"
        self.yes.set_selected(False)
        self.no.set_selected(True)

    def right_press(self):
        self.selected_button = "yes"
        self.yes.set_selected(True)
        self.no.set_selected(False)

    def popup(self, correct):
        if correct:
            self.set_image(self.correct_popup, 749, 450)
        else:
            self.set_image(self.incorrect_popup, 749, 450)

        self.set_timer(45, self.end_popup)

    def end_popup(self):
        if self.current_question != len(self.answers):
            image = self.load_image(self.questions[self.current_question])
            self.set_image(image, 900, 450)
            self.can_select = True

    def select(self):
        if self.can_select:
            self.can_select = False
            if self.yes.selected:
                if self.answers[self.current_question]:
                    self.popup(True)
                else:
                    self.popup(False)
            else:
                if self.answers[self.current_question]:
                    self.popup(False)
                else:
                    self.popup(True)
            self.current_question += 1
            if self.current_question == len(self.answers):
                self.set_timer(45, self.end_room)

    def end_room(self):
        print("All questions finished")
        Globals.next_level = EnumLevels.FinishQuiz
        self.room.running = False

    def key_pressed(self, key):
        if key[pygame.K_RIGHT]:
            self.right_press()
        elif key[pygame.K_LEFT]:
            self.left_press()
        elif key[pygame.K_b]:
            self.end_room()
        elif key[pygame.K_SPACE]:
            self.select()

    def joy_pad_signal(self, p1_buttons, p2_buttons):
        if p1_buttons[11] > 0.5:
            self.right_press()
        elif p1_buttons[11] < -0.5:
            self.left_press()
        elif p1_buttons[2]:
            self.select()
