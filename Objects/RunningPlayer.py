from GameFrame import RoomObject, Globals
from Objects.BonusPoints import BonusPoints
import pygame
import os


class RunningPlayer(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)
        self.width = 64
        self.height = 64
        self.set_image(self.load_image(os.path.join("Escape", "Player Right 1.png")), self.width, self.height)
        self.animation_folder = "Escape"
        self.controller = "gamepad"

        # animations are held in arrays
        self.run_animation = ["Player Right 1.png",
                              "Player Right 2.png",
                              "Player Right 3.png",
                              "Player Right 4.png",
                              "Player Right 5.png",
                              "Player Right 6.png"]

        self.crouch_animation = ["Player Right crouch 1.png",
                                 "Player Right crouch 2.png",
                                 "Player Right crouch 3.png",
                                 "Player Right crouch 4.png",
                                 "Player Right crouch 5.png",
                                 "Player Right crouch 6.png"]

        self.animation = self.run_animation
        self.playing = True
        self.y_velocity = 0
        self.handle_key_events = True
        self.gravity_force = 1  # starting force of gravity
        self.terminal_velocity = 6  # maximum downward speed
        self.jump_force = 14  # determines how high the player jumps
        self.control_enabled = True
        self.flashing = False
        self.invincibility_frames = 120
        self.flashing_countdown = 0

        self.register_collision_object("Obstacle")

    def step(self):

        # crouching code

        # i have to do this annoying controller checking
        # otherwise the game just crashes if you don't have a controller connected :))))
        if self.controller == "gamepad":
            if len(self.room.p1_btns) >= 11:
                if self.room.p1_btns[10] > 0.5 and self.y >= 720 - 128 - self.height:
                    self.animation = self.crouch_animation
                    self.y = 720 - 128 - self.height
                else:
                    self.animation = self.run_animation
        elif self.controller == "keyboard":
            if self.is_key_pressed(pygame.K_DOWN) and self.y >= 720 - 128 - self.height:
                self.animation = self.crouch_animation
                self.y = 720 - 128 - self.height
            else:
                self.animation = self.run_animation

        # handles invincibility frames
        if self.flashing:
            self.visible = not self.visible
            self.flashing_countdown += 1
        else:
            self.visible = True
        if self.flashing_countdown >= self.invincibility_frames:
            self.flashing = False
            self.flashing_countdown = 0

        # update position
        self.y += round(self.y_velocity)  # we have to round the velocity otherwise weird stuff happens lol

        # reset values when touching the ground
        if self.y >= 720 - 128 - self.height:
            self.gravity_force = 1
            self.y_velocity = 0
            self.y = 720 - 128 - self.height

        # applying gravity to the player
        if self.y < 720 - 128 - self.height:
            # this makes sure the player doesn't go under the floor
            if self.y + self.gravity_force >= 528:
                self.y += self.y - 528
                self.y_velocity = 0
            else:
                self.y_velocity += self.gravity_force

            # clamp the player's downward speed
            if self.gravity_force < self.terminal_velocity:
                self.gravity_force += 0.1

    def is_key_pressed(self, key):
        keys = pygame.key.get_pressed()
        if keys[key]:
            return True
        else:
            return False

    def key_pressed(self, key):

        # jump if on the floor
        if key[pygame.K_SPACE] and self.y >= 720 - 128 - self.height and self.control_enabled:
            self.room.jump_sound.play()
            self.y_velocity -= self.jump_force
            self.controller = "keyboard"

        # this makes it so holding space longer = higher jump
        if key[pygame.K_SPACE] and self.y_velocity < 0 and self.control_enabled:
            self.y_velocity *= 1.1
            self.controller = "keyboard"

        if key[pygame.K_DOWN]:
            self.controller = "keyboard"

    def joy_pad_signal(self, p1_buttons, p2_buttons):
        self.controller = "gamepad"
        if p1_buttons[1] and self.y >= 720 - 128 - self.height and self.control_enabled:
            self.room.jump_sound.play()
            self.y_velocity -= self.jump_force

        if p1_buttons[1] and self.y_velocity < 0 and self.control_enabled:
            self.y_velocity *= 1.1

    def handle_collision(self, other, other_type):
        # take damage from hitting obstacles
        if other_type == "Obstacle" and not self.flashing and not other.is_heart:
            # hard coding it to not collide with the crouching log when you're crouching because im bad at programming
            if self.animation == self.crouch_animation and other.obstacle == "Crouch Log":
                return
            self.flashing = True
            other.visible = False
            Globals.LIVES -= 1
            self.room.crash_sound.play()
        # replenish health from hitting a heart
        if other_type == "Obstacle" and other.is_heart and other.visible:
            other.visible = False
            if Globals.LIVES < 3:
                Globals.LIVES += 1
            elif Globals.LIVES == 3:
                self.room.dist_travelled += 200
                self.room.add_room_object(BonusPoints(self.room, self.x, self.y))
            self.room.health_sound.play()
