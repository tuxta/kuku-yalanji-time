import pygame
from GameFrame import RoomObject, Globals, EnumLevels
import os


class ForagingPlayer(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        self.player_image = self.load_image(os.path.join('Foraging', 'front_1.png'))

        self.image_width = 36
        self.image_height = 44
        self.cutscene = False
        self.set_image(self.player_image, self.image_width, self.image_height)

        # Load player animation images
        self.down = []
        self.down.append(self.load_image(os.path.join('Foraging', 'front_2.png')))
        self.down.append(self.load_image(os.path.join('Foraging', 'front_3.png')))
        self.up = []
        self.up.append(self.load_image(os.path.join('Foraging', 'back_1.png')))
        self.up.append(self.load_image(os.path.join('Foraging', 'back_2.png')))
        self.left = []
        self.left.append(self.load_image(os.path.join('Foraging', 'left_1.png')))
        self.left.append(self.load_image(os.path.join('Foraging', 'left_2.png')))
        self.right = []
        self.right.append(self.load_image(os.path.join('Foraging', 'right_1.png')))
        self.right.append(self.load_image(os.path.join('Foraging', 'right_2.png')))

        self.img_index = 0

        self.LEFT = 0
        self.RIGHT = 1
        self.UP = 2
        self.DOWN = 3

        self.facing = 4

        self.handle_key_events = True

        self.register_collision_object('Block')
        self.register_collision_object('Tree')
        self.register_collision_object('Gunya')
        self.register_collision_object('LargeTree')
        self.register_collision_object('BlockBottom')

        self.block_right = False
        self.block_left = False
        self.block_up = False
        self.block_down = False

        self.moving = False

        self.animate_custom()

    def prestep(self):
        self.block_right = False
        self.block_left = False
        self.block_up = False
        self.block_down = False
        self.facing = 4

    def handle_collision(self, other, other_type):
        if other_type == 'Block' or other_type == 'Tree' or other_type == 'Gunya' or other_type == 'LargeTree' \
                or other_type == 'BlockBottom':

            if (
                    self.collides_at(self, 4, 0, 'Block') or

                    (
                            self.collides_at(self, 4, 0, 'Gunya') or

                            self.collides_at(self, 4, 0, 'BlockBottom') or

                            self.collides_at(self, 4, 0, 'LargeTree') and self.rect.bottom > other.y + 80 or

                            self.collides_at(self, 4, 0, 'Tree') and self.rect.bottom > other.y + 80
                    )
            ) and not self.block_right:
                self.block_right = True
                if self.x < 596:
                    self.x = self.prev_x
                else:
                    self.move_left()

            if (
                    self.collides_at(self, -4, 0, 'Block') or

                    (
                            self.collides_at(self, -4, 0, 'Gunya') or

                            self.collides_at(self, -4, 0, 'BlockBottom') or

                            self.collides_at(self, -4, 0, 'LargeTree') and self.rect.bottom > other.y + 80 or

                            self.collides_at(self, -4, 0, 'Tree') and self.rect.bottom > other.y + 80
                    )
            ) and not self.block_left:
                self.block_left = True
                if self.x >= 206:
                    self.x = self.prev_x
                else:
                    self.move_right()

            if (
                    self.collides_at(self, 0, 4, 'Block') or

                    (
                            self.collides_at(self, 0, 4, 'Gunya') or

                            self.collides_at(self, 0, 4, 'BlockBottom') or

                            self.collides_at(self, 0, 4, 'LargeTree') and self.rect.bottom > other.y + 80 or

                            self.collides_at(self, 0, 4, 'Tree') and self.rect.bottom > other.y + 80
                    )
            ):
                self.block_down = True
                if self.y <= 446:
                    self.y = self.prev_y
                else:
                    self.move_up()

            if (
                    self.collides_at(self, 0, -4, 'Block') or

                    (
                            self.collides_at(self, 0, -4, 'Gunya') or

                            self.collides_at(self, 0, -4, 'BlockBottom') or

                            self.collides_at(self, 0, -4, 'LargeTree') and self.rect.bottom > other.y + 80 or

                            self.collides_at(self, 0, -4, 'Tree') and self.rect.bottom > other.y + 80
                    )
            ) and not self.block_up:
                self.block_up = True
                if self.y >= 154:
                    self.y = self.prev_y
                else:
                    self.move_down()

    def key_pressed(self, key):
        if not Globals.popup_active:
            if key[pygame.K_LEFT]:
                self.move_left()
                self.facing = self.LEFT
            elif key[pygame.K_RIGHT]:
                self.move_right()
                self.facing = self.RIGHT
            elif key[pygame.K_UP]:
                self.move_up()
                self.facing = self.UP
            elif key[pygame.K_DOWN]:
                self.move_down()
                self.facing = self.DOWN
            elif key[pygame.K_n]:
                self.room.end_room()

    def joy_pad_signal(self, p1_buttons, p2_buttons):
        if not Globals.popup_active:
            if p1_buttons[11] < -0.5:
                self.move_left()
                self.facing = self.LEFT
            elif p1_buttons[11] > 0.5:
                self.move_right()
                self.facing = self.RIGHT

            if p1_buttons[10] > 0.5:
                self.move_down()
                self.facing = self.DOWN
            elif p1_buttons[10] < -0.5:
                self.move_up()
                self.facing = self.UP
            elif p1_buttons[3]:
                self.room.end_room()

    def move_right(self):
        if self.x < 600:
            self.x += Globals.move_speed
        else:
            self.room.shift_room_left()

    def move_left(self):
        if self.x > 200:
            self.x -= Globals.move_speed
        else:
            self.room.shift_room_right()

    def move_up(self):
        if self.y > 150:
            self.y -= Globals.move_speed
        else:
            self.room.shift_room_down()

    def move_down(self):
        if self.y < 450:
            self.y += Globals.move_speed
        else:
            self.room.shift_room_up()

    def animate_custom(self):
        self.img_index += 1
        self.img_index %= 2
        image_width = 42
        image_height = 50
        if self.facing == self.LEFT:
            self.set_image(self.left[self.img_index], image_width, image_height)
        elif self.facing == self.RIGHT:
            self.set_image(self.right[self.img_index], image_width, image_height)
        elif self.facing == self.UP:
            self.set_image(self.up[self.img_index], image_width, image_height)
        elif self.facing == self.DOWN:
            self.set_image(self.down[self.img_index], image_width, image_height)
        else:
            self.set_image(self.player_image, image_width, image_height)

        self.set_timer(6, self.animate_custom)
