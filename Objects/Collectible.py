from GameFrame import RoomObject, Globals
from Objects.InfoPopup import InfoPopup
from Objects.CollectibleIcon import CollectibleIcon
import os


class Collectible(RoomObject):
    def __init__(self, room, x, y, collectible_type):
        RoomObject.__init__(self, room, x, y)

        self.collectible_type = collectible_type
        image_width = 32
        image_length = 32

        self.img_file = ''
        self.popup_image = ''

        if collectible_type == 'turkey':
            self.img_file = os.path.join('Foraging', 'F_Turkey.png')
            self.popup_image = self.load_image(os.path.join('Foraging', 'Info Australian Brush-Turkey.png'))
        elif collectible_type == 'cassowary':
            self.img_file = os.path.join('Foraging', 'F_Cassowary.png')
            self.popup_image = self.load_image(os.path.join('Foraging', 'Info Cassowary.png'))
        elif collectible_type == 'crocodile':
            self.img_file = os.path.join('Foraging', 'F_Crocodile.png')
            self.popup_image = self.load_image(os.path.join('Foraging', 'Info Crocodile, Freshwater.png'))
            image_width = 53
            image_length = 24
        elif collectible_type == 'mullet':
            self.img_file = os.path.join('Foraging', 'F_Mullet.png')
            self.popup_image = self.load_image(os.path.join('Foraging', 'Info Mullet.png'))
        elif collectible_type == 'drop vine':
            self.img_file = os.path.join('Foraging', 'F_AcidVine.png')
            self.popup_image = self.load_image(os.path.join('Foraging', 'Info Acid Drop Vine.png'))
        elif collectible_type == 'quandong':
            self.img_file = os.path.join('Foraging', 'F_Quandong.png')
            self.popup_image = self.load_image(os.path.join('Foraging', 'Info Blue Quandong.png'))
        elif collectible_type == 'plum':
            self.img_file = os.path.join('Foraging', 'F_Plum.png')
            self.popup_image = self.load_image(os.path.join('Foraging', 'Info Davidsons Plum.png'))
        elif collectible_type == 'banana':
            self.img_file = os.path.join('Foraging', 'F_Banana.png')
            self.popup_image = self.load_image(os.path.join('Foraging', 'Info Native Banana.png'))
        elif collectible_type == 'beech':
            self.img_file = os.path.join('Foraging', 'F_Red Beech.png')
            self.popup_image = self.load_image(os.path.join('Foraging', 'Info Red Beech.png'))
        elif collectible_type == 'turtle':
            self.img_file = os.path.join('Foraging', 'Turtle.png')
            self.popup_image = self.load_image(os.path.join('Foraging', 'Info Native Banana.png'))
        elif collectible_type == 'gunya':
            self.img_file = os.path.join('Foraging', 'F_Gunya.png')
            self.popup_image = self.load_image(os.path.join('Foraging', 'Info Gunya.png'))
            image_width = 112
            image_length = 84

        image = self.load_image(self.img_file)
        self.set_image(image, image_width, image_length)

        self.register_collision_object("ForagingPlayer")
        self.in_collision = False

    def handle_collision(self, other, other_type):
        if not self.in_collision:
            Globals.FaunaFlora_found += 1
            self.room.collectible_sound.play()

            my_pop_up = InfoPopup(self.room, 50, 50, self.popup_image)
            self.room.add_room_object(my_pop_up)
            Globals.popup_active = True

            icon = CollectibleIcon(self.room, Globals.icon_bar_length, 40, self.img_file)
            self.room.add_room_object(icon)
            Globals.icon_bar_length += 52

            self.delete_object(self)
            self.x = self.prev_x
            self.y = self.prev_y
            self.in_collision = True
            self.set_timer(4, self.reset_collision)

    def reset_collision(self):
        self.in_collision = False
