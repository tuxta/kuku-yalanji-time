from GameFrame import TextObject
import pygame


class ScoreText(TextObject):
    def __init__(self, room, x, y):
        TextObject.__init__(self, room, x, y)
        self.depth = 30000
        self.size = 60
        self.update_text_custom()

    # update_text() but with my own custom font because it looks nice :)
    def update_text_custom(self):
        self.text = 'Score:' + str(self.room.dist_travelled)
        self.built_font = pygame.font.Font('Objects/VCR_OSD_MONO.ttf', self.size)
        self.rendered_text = self.built_font.render(self.text, False, self.colour)
        self.image = self.rendered_text
        self.width, self.height = self.built_font.size(self.text)
        self.rect = pygame.Rect(self.x, self.y, self.width, self.height)