import pygame, os
from GameFrame import RoomObject, Globals


class Player(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        # - The width and height have been set here for later reference in the room - #
        self.width = 72
        self.height = 84
        self.depth = 1

        self.player_sprite_front = self.load_image(os.path.join('Fight', 'Player Sprite Front.png'))
        self.set_image(self.player_sprite_front, self.width, self.height)

        # - Loads images into a list for use in animation - #
        self.leftCycle = [
            self.load_image(os.path.join('Fight', 'Player Left 1.png')),
            self.load_image(os.path.join('Fight', 'Player Left 2.png')),
            self.load_image(os.path.join('Fight', 'Player Left 3.png')),
            self.load_image(os.path.join('Fight', 'Player Left 4.png')),
            self.load_image(os.path.join('Fight', 'Player Left 5.png')),
            self.load_image(os.path.join('Fight', 'Player Left 6.png'))
        ]

        self.rightCycle = [
            self.load_image(os.path.join('Fight', 'Player Right 1.png')),
            self.load_image(os.path.join('Fight', 'Player Right 2.png')),
            self.load_image(os.path.join('Fight', 'Player Right 3.png')),
            self.load_image(os.path.join('Fight', 'Player Right 4.png')),
            self.load_image(os.path.join('Fight', 'Player Right 5.png')),
            self.load_image(os.path.join('Fight', 'Player Right 6.png'))
        ]

        self.upCycle = [
            self.load_image(os.path.join('Fight', 'Player Up 1.png')),
            self.load_image(os.path.join('Fight', 'Player Up 2.png')),
            self.load_image(os.path.join('Fight', 'Player Up 3.png')),
            self.load_image(os.path.join('Fight', 'Player Up 4.png')),
            self.load_image(os.path.join('Fight', 'Player Up 5.png')),
            self.load_image(os.path.join('Fight', 'Player Up 6.png'))
        ]

        self.downCycle = [
            self.load_image(os.path.join('Fight', 'Player Down 1.png')),
            self.load_image(os.path.join('Fight', 'Player Down 2.png')),
            self.load_image(os.path.join('Fight', 'Player Down 3.png')),
            self.load_image(os.path.join('Fight', 'Player Down 4.png')),
            self.load_image(os.path.join('Fight', 'Player Down 5.png')),
            self.load_image(os.path.join('Fight', 'Player Down 6.png'))
        ]

        self.curr_index = 0

        self.right = self.load_image(os.path.join('Fight', 'Player Sprite Right.png'))
        self.left = self.load_image(os.path.join('Fight', 'Player Sprite Left.png'))
        self.down = self.load_image(os.path.join('Fight', 'Player Sprite Front.png'))
        self.up = self.load_image(os.path.join('Fight', 'Player Sprite Back.png'))

        # - These values are set here for later reference in the object - #
        self.direction = 'right'
        self.shootDelay = 0
        self.health = 3
        self.velocity = 10
        self.invulnerable = False
        self.dodge_cooldown = False
        self.dodge_active = False
        self.ammoCount = 6

        # - These values are used to animate the player sprite - #
        self.walkLeft = False
        self.walkRight = True
        self.walkUp = False
        self.walkDown = False
        self.walkUpLeft = False
        self.walkUpRight = False
        self.walkDownLeft = False
        self.walkDownRight = False
        self.standing = True

        # - Sets the player image to create a rect - #
        self.set_image(self.rightCycle[self.curr_index // 2], self.width, self.height)

        # - Allows the object to detect key presses - #
        self.handle_key_events = True

        # - Registers specified objects as possible collision objects - #
        self.register_collision_object('Enemy')
        self.register_collision_object('DashEnemy')
        self.register_collision_object('AmmoBox')
        self.register_collision_object('EnemyFullBullet')
        self.register_collision_object('EnemySplitBullet')

        self.animate()

    def prestep(self):
        if self.standing:
            if self.direction == 'left' or self.direction == 'upleft' or self.direction == 'downleft':
                self.set_image(self.left, self.width, self.height)
            if self.direction == 'right' or self.direction == 'downleft' or self.direction == 'downright':
                self.set_image(self.right, self.width, self.height)
            if self.direction == 'up':
                self.set_image(self.up, self.width, self.height)
            if self.direction == 'down':
                self.set_image(self.down, self.width, self.height)

    # - The four following functions are used to move the player across the screen - #
    def moveLeft(self):
        self.direction = 'left'
        self.walkLeft = True
        self.walkRight = False
        self.walkUp = False
        self.walkDown = False
        self.walkUpLeft = False
        self.walkUpRight = False
        self.walkDownLeft = False
        self.walkDownRight = False
        self.standing = False
        self.x -= self.velocity

    def moveRight(self):
        self.direction = 'right'
        self.walkLeft = False
        self.walkRight = True
        self.walkUp = False
        self.walkDown = False
        self.walkUpLeft = False
        self.walkUpRight = False
        self.walkDownLeft = False
        self.walkDownRight = False
        self.standing = False
        self.x += self.velocity

    def moveUp(self):
        self.direction = 'up'
        self.walkLeft = False
        self.walkRight = False
        self.walkUp = True
        self.walkDown = False
        self.walkUpLeft = False
        self.walkUpRight = False
        self.walkDownLeft = False
        self.walkDownRight = False
        self.standing = False
        self.y -= self.velocity

    def moveDown(self):
        self.direction = 'down'
        self.walkLeft = False
        self.walkRight = False
        self.walkUp = False
        self.walkDown = True
        self.walkUpLeft = False
        self.walkUpRight = False
        self.walkDownLeft = False
        self.walkDownRight = False
        self.standing = False
        self.y += self.velocity

    def moveupleft(self):
        self.direction = 'upleft'
        self.walkLeft = False
        self.walkRight = False
        self.walkUp = False
        self.walkDown = False
        self.walkUpLeft = True
        self.walkUpRight = False
        self.walkDownLeft = False
        self.walkDownRight = False
        self.standing = False
        self.y -= self.velocity / 2
        self.x -= self.velocity / 2

    def moveupright(self):
        self.direction = 'upright'
        self.walkLeft = False
        self.walkRight = False
        self.walkUp = False
        self.walkDown = False
        self.walkUpLeft = False
        self.walkUpRight = True
        self.walkDownLeft = False
        self.walkDownRight = False
        self.standing = False
        self.y -= self.velocity / 2
        self.x += self.velocity / 2

    def movedownleft(self):
        self.direction = 'downleft'
        self.walkLeft = False
        self.walkRight = False
        self.walkUp = False
        self.walkDown = False
        self.walkUpLeft = False
        self.walkUpRight = False
        self.walkDownLeft = True
        self.walkDownRight = False
        self.standing = False
        self.y += self.velocity / 2
        self.x -= self.velocity / 2

    def movedownright(self):
        self.direction = 'downright'
        self.walkLeft = False
        self.walkRight = False
        self.walkUp = False
        self.walkDown = False
        self.walkUpLeft = False
        self.walkUpRight = False
        self.walkDownLeft = False
        self.walkDownRight = True
        self.standing = False
        self.y += self.velocity / 2
        self.x += self.velocity / 2

    # - This is called when the player is not moving - #
    def idle(self):
        self.standing = True
        self.curr_index = 0

    # - This is used to animate the player - #
    def animate(self):
        if not self.standing:
            if self.walkLeft or self.walkDownLeft or self.walkUpLeft:
                self.set_image(self.leftCycle[self.curr_index], self.width, self.height)
            elif self.walkRight or self.walkDownRight or self.walkUpRight:
                self.set_image(self.rightCycle[self.curr_index], self.width, self.height)
            elif self.walkUp:
                self.set_image(self.upCycle[self.curr_index], self.width, self.height)
            elif self.walkDown:
                self.set_image(self.downCycle[self.curr_index], self.width, self.height)

            self.curr_index += 1
            self.curr_index %= 6

        else:
            if self.walkLeft:
                self.set_image(self.left, self.width, self.height)
            elif self.walkRight:
                self.set_image(self.right, self.width, self.height)
            elif self.walkUp:
                self.set_image(self.up, self.width, self.height)
            else:
                self.set_image(self.down, self.width, self.height)

        self.set_timer(5, self.animate)

    # - Calls the 'shoot' function in the BossRoom level, only if the shoot cooldown is over - #
    def shoot(self):
        if self.ammoCount > 0:
            if self.shootDelay <= 0:
                self.room.shoot(self.direction)
                # - This creates a 15 frame delay between each shot (0.5s) - #
                self.shootDelay = 30
                self.ammoCount -= 1
                self.room.AmmoText.text = "Ammunition: " + str(self.ammoCount)
                self.room.AmmoText.update_text()

    def dodge(self):
        self.cooldown_length = 30
        if self.dodge_cooldown == False:
            self.dodge_length = 10
            self.velocity = 15
            self.invulnerable = True
            self.set_timer(self.dodge_length, self.resetDamage)
            self.set_timer(self.dodge_length, self.resetVelocity)
            self.dodge_cooldown = True
            self.set_timer(self.cooldown_length, self.resetCooldown)
            self.dodge_active = True
            self.set_timer(self.dodge_length, self.deactivateDodge)

    # - This causes the player to take damage if the invulnerability timer has run out - #
    # - It then checks if the player's health is 0 or less. If this is true, it ends the game - #
    def takeDamage(self):
        if not self.invulnerable:
            self.invulnerable = True
            self.health -= 1
            print (self.health)
            if self.health <= 0:
                self.room.endGame()
            self.set_timer(30, self.resetDamage)

    # - This turns off the invulnerability of the player - #
    def resetDamage(self):
        self.invulnerable = False

    def resetVelocity(self):
        self.velocity = 10

    def resetCooldown(self):
        self.dodge_cooldown = False

    def deactivateDodge(self):
        self.dodge_active = False

    def addAmmo(self):
        self.ammoCount += 2
        self.room.AmmoText.text = "Ammunition: " + str(self.ammoCount)
        self.room.AmmoText.update_text()

    def key_pressed(self, key):
        # - Checks for the movement keys (w, a, s, d) and calls the appropriate functions - #
        if self.dodge_active == False:
            if (key[pygame.K_w] or key[pygame.K_UP]) and self.y > self.velocity:
                if key[pygame.K_a] or key[pygame.K_LEFT] and self.x > self.velocity:
                    self.moveupleft()
                elif key[pygame.K_d] or key[pygame.K_RIGHT] and self.x < Globals.SCREEN_WIDTH - self.height - self.velocity:
                    self.moveupright()
                else:
                    self.moveUp()
            elif (key[pygame.K_s] or key[pygame.K_DOWN]) and self.y < Globals.SCREEN_HEIGHT - self.height - self.velocity:
                if key[pygame.K_a] or key[pygame.K_LEFT] and self.x > self.velocity:
                    self.movedownleft()
                elif key[pygame.K_d] or key[pygame.K_RIGHT] and self.x < Globals.SCREEN_WIDTH - self.width - self.velocity:
                    self.movedownright()
                else:
                    self.moveDown()
            elif (key[pygame.K_a] or key[pygame.K_LEFT]) and self.x > self.velocity:
                self.moveLeft()
            elif (key[pygame.K_d] or key[pygame.K_RIGHT]) and self.x < Globals.SCREEN_WIDTH - self.width - self.velocity:
                self.moveRight()
            else:
                self.idle()
        # - Calls the player's 'shoot' function when the Space key is pressed - #
        if key[pygame.K_SPACE]:
            self.shoot()
        # - Calls the player's 'melee' function when the X key is pressed - #
        if key[pygame.K_RALT]:
            self.room.Spear.melee()
        # - Calls the player's 'dodge' function when the Shift key is pressed - #
        if key[pygame.K_LSHIFT]:
            self.dodge()

    def joy_pad_signal(self, p1_buttons, p2_buttons):
        if p1_buttons[10] < -0.5 and self.y > self.velocity:
            self.moveUp()
        elif p1_buttons[11] < -0.5 and self.x > self.velocity:
            self.moveLeft()
        elif p1_buttons[10] > 0.5 and self.y < Globals.SCREEN_HEIGHT - self.height - self.velocity:
            self.moveDown()
        elif p1_buttons[11] > 0.5 and self.x < Globals.SCREEN_WIDTH - self.width - self.velocity:
            self.moveRight()
        else:
            self.idle()

        if p1_buttons[4]:
            self.dodge()

        if p1_buttons[5]:
            self.room.Spear.melee()

        if p1_buttons[2]:
            self.shoot()

    # - Minuses one from the shootDelay variable each frame - #
    def step(self):
        self.shootDelay -= 1

        if self.dodge_active == True:
            if self.walkLeft == True:
                self.moveLeft()
            if self.walkUp == True:
                self.moveUp()
            if self.walkRight == True:
                self.moveRight()
            if self.walkDown == True:
                self.moveDown()
            if self.walkUpLeft == True:
                self.moveupleft()
            if self.walkUpRight == True:
                self.moveupright()
            if self.walkDownLeft == True:
                self.movedownleft()
            if self.walkDownRight == True:
                self.movedownright()

    # - Handles and checks for collisions with other objects - #
    # - Note: These objects MUST be predefined in the script for the function to work - #
    def handle_collision(self, other, other_type):
        if other_type == 'Enemy' or other_type == 'EnemyFullBullet' or other_type == 'EnemySplitBullet':
            self.takeDamage()
        if other_type == 'AmmoBox':
            self.addAmmo()
