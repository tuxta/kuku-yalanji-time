from GameFrame import RoomObject
import os


class BonusPoints(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)
        self.bp_image = self.load_image(os.path.join("Escape", "bonus_points.png"))
        self.set_image(self.bp_image, 96, 32)

        self.depth = 100000
        self.speed = 5
        self.distance = 50
        self.ending_y = y - self.distance

    def step(self):
        if self.y > self.ending_y:
            self.y -= self.speed
        else:
            self.room.delete_object(self)