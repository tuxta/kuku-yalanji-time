from GameFrame import RoomObject
import os


class DK_Block(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        image = self.load_image(os.path.join('DK', 'block.png'))
        self.set_image(image, 32, 32)
