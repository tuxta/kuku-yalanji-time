from GameFrame import RoomObject, Globals, EnumLevels
import os


class QuizButton(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        self.selected_image = self.load_image(os.path.join('InfoPage', 'QuizHighlight.png'))
        self.unselected_image = self.load_image(os.path.join('InfoPage', 'QuizButton.PNG'))

        self.set_image(self.unselected_image, 300, 200)

    def set_selected(self, selected):
        if selected:
            self.set_image(self.selected_image, 300, 200)
        else:
            self.set_image(self.unselected_image, 300, 200)





