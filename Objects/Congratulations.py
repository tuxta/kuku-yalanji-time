from GameFrame import RoomObject
import os


class Congratulations(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        image = self.load_image(os.path.join('InfoPage', 'Congratulations.png'))
        self.set_image(image, 749, 450)