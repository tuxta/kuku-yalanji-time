from GameFrame import RoomObject
import os


class Enemy_Health_Bar(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        self.height = 22

        self.length_50 = 1050
        self.barHealth = 50

        self.image1 = self.load_image(os.path.join('Fight', 'HealthBar1.png'))
        self.image2 = self.load_image(os.path.join('Fight', 'HealthBar2.png'))
        self.set_image(self.image1, self.length_50, self.height)
        self.depth = 100

    def change_length(self):
        if self.room.enemy_health < self.barHealth:
            self.length_50 = (self.room.enemy_health * 21)
            self.barHealth -= 1
            if self.room.enemy_health > 25:
                self.set_image(self.image1, self.length_50, self.height)
            elif self.room.enemy_health > 5:
                self.set_image(self.image2, self.length_50, self.height)
            else:
                self.set_image(self.image2, 5, self.height)

    def step(self):
        self.change_length()
