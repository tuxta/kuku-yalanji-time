from GameFrame import RoomObject
import pygame


class NextButton(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        image = self.load_image("press_a.png")
        self.set_image(image, 128, 128)

        self.handle_key_events = True

    def key_pressed(self, key):
        if key[pygame.K_SPACE]:
            self.room.next_background()
            self.delete_object(self)

    def joy_pad_signal(self, p1_buttons, p2_buttons):
        if p1_buttons[1]:
            self.room.next_background()
            self.delete_object(self)
