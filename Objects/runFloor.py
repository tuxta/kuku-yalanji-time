from GameFrame import RoomObject
import os

class RunFloor(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        self.starting_x = self.x

        self.img = self.load_image(os.path.join("Escape", "runFloor.png"))
        self.set_image(self.img, 1280, 128)

    def step(self):

        # moves the floor back and then loops back once fully offscreen
        # should be used with a second floor so as to create no gaps
        if self.x > self.starting_x - 1280:
            self.x -= self.room.move_speed
        else:
            self.x = self.starting_x