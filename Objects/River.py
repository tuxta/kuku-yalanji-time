from GameFrame import RoomObject
import os


class River(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        image = self.load_image(os.path.join('Foraging', 'River.png'))
        self.set_image(image, 130, 2160)
        self.depth = -50
