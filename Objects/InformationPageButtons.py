from GameFrame import RoomObject, Globals, EnumLevels
import pygame
import os


class InformationPageButtons(RoomObject):
    def __init__(self, room, x, y, wurrumbu_button, about_button, manjal_button, kubirri_button, kbbutton, quiz_button):
        RoomObject.__init__(self, room, x, y)

        image = self.load_image(os.path.join('InfoPage', 'Information.png'))
        self.set_image(image, 500, 100)

        self.about_button = about_button
        self.wurrumbu_button = wurrumbu_button
        self.manjal_button = manjal_button
        self.kubirri_button = kubirri_button
        self.kbbutton = kbbutton
        self.quiz_button = quiz_button

        self.selected_button = "manjal_button"
        self.handle_key_events = True

    def right_press(self):
        if self.selected_button == "manjal_button":
            self.selected_button = "wurrumbu_button"
            self.manjal_button.set_selected(False)
            self.wurrumbu_button.set_selected(True)
        elif self.selected_button == "about_button":
            self.selected_button = "manjal_button"
            self.about_button.set_selected(False)
            self.manjal_button.set_selected(True)
        elif self.selected_button == "wurrumbu_button":
            self.selected_button = "wurrumbu_button"
            self.manjal_button.set_selected(False)
            self.wurrumbu_button.set_selected(True)
            self.about_button.set_selected(False)
        elif self.selected_button == "kubirri_button":
            self.selected_button = "quiz_button"
            self.kubirri_button.set_selected(False)
            self.quiz_button.set_selected(True)
        elif self.selected_button == "quiz_button":
            self.selected_button = "kbbutton"
            self.quiz_button.set_selected(False)
            self.kbbutton.set_selected(True)
        elif self.selected_button == "kbbutton":
            self.selected_button = "kbbutton"
            self.quiz_button.set_selected(False)
            self.kubirri_button.set_selected(False)
            self.kbbutton.set_selected(True)
            self.about_button.set_selected(False)
            self.wurrumbu_button.set_selected(False)
            self.manjal_button.set_selected(False)
        else:
            self.selected_button == "manjal_button"
            self.selected_button = "manjal_button"
            self.quiz_button.set_selected(False)
            self.kubirri_button.set_selected(False)
            self.kbbutton.set_selected(False)
            self.about_button.set_selected(False)
            self.wurrumbu_button.set_selected(False)
            self.manjal_button.set_selected(True)

    def left_press(self):
        if self.selected_button == "manjal_button":
            self.selected_button = "about_button"
            self.manjal_button.set_selected(False)
            self.about_button.set_selected(True)
        elif self.selected_button == "wurrumbu_button":
            self.selected_button = "manjal_button"
            self.wurrumbu_button.set_selected(False)
            self.manjal_button.set_selected(True)
        elif self.selected_button == "about_button":
            self.selected_button = "about_button"
            self.manjal_button.set_selected(False)
            self.wurrumbu_button.set_selected(False)
            self.about_button.set_selected(True)
        elif self.selected_button == "quiz_button":
            self.selected_button = "kubirri_button"
            self.quiz_button.set_selected(False)
            self.kubirri_button.set_selected(True)
        elif self.selected_button == "kbbutton":
            self.selected_button = "quiz_button"
            self.kbbutton.set_selected(False)
            self.quiz_button.set_selected(True)
        elif self.selected_button == "kubirri_button":
            self.selected_button = "kubirri_button"
            self.kubirri_button.set_selected(True)
            self.quiz_button.set_selected(False)
            self.kbbutton.set_selected(False)
        else:
            self.selected_button == "manjal_button"
            self.selected_button = "manjal_button"
            self.quiz_button.set_selected(False)
            self.kubirri_button.set_selected(False)
            self.kbbutton.set_selected(False)
            self.about_button.set_selected(False)
            self.wurrumbu_button.set_selected(False)
            self.manjal_button.set_selected(True)

    def down_press(self):
        if self.selected_button == "about_button":
            self.selected_button = "kubirri_button"
            self.about_button.set_selected(False)
            self.kubirri_button.set_selected(True)
        elif self.selected_button == "manjal_button":
            self.selected_button = "quiz_button"
            self.manjal_button.set_selected(False)
            self.quiz_button.set_selected(True)
        elif self.selected_button == "wurrumbu_button":
            self.selected_button = "kbbutton"
            self.wurrumbu_button.set_selected(False)
            self.kbbutton.set_selected(True)
        elif self.selected_button == "kubirri_button":
            self.selected_button = "kubirri_button"
            self.kubirri_button.set_selected(True)
        elif self.selected_button == "quiz_button":
            self.selected_button = "quiz_button"
            self.quiz_button.set_selected(True)
        elif self.selected_button == "kbbutton":
            self.selected_button = "kbbutton"
            self.quiz_button.set_selected(False)
            self.kubirri_button.set_selected(False)
            self.kbbutton.set_selected(True)
            self.about_button.set_selected(False)
            self.wurrumbu_button.set_selected(False)
            self.manjal_button.set_selected(False)
        else:
            self.selected_button == "manjal_button"
            self.selected_button = "manjal_button"
            self.quiz_button.set_selected(False)
            self.kubirri_button.set_selected(False)
            self.kbbutton.set_selected(False)
            self.about_button.set_selected(False)
            self.wurrumbu_button.set_selected(False)
            self.manjal_button.set_selected(True)



    def up_press(self):
        if self.selected_button == "kubirri_button":
            self.selected_button = "about_button"
            self.kubirri_button.set_selected(False)
            self.about_button.set_selected(True)
        elif self.selected_button == "quiz_button":
            self.selected_button = "manjal_button"
            self.quiz_button.set_selected(False)
            self.manjal_button.set_selected(True)
        elif self.selected_button == "kbbutton":
            self.selected_button = "wurrumbu_button"
            self.kbbutton.set_selected(False)
            self.wurrumbu_button.set_selected(True)

    def select_press(self):
        if self.selected_button == "manjal_button":
            Globals.next_level = EnumLevels.ManjalPage
        elif self.selected_button == "wurrumbu_button":
            Globals.next_level = EnumLevels.WurrumbuPage
        elif self.selected_button == "about_button":
            Globals.next_level = EnumLevels.About
        elif self.selected_button == "kubirri_button":
            Globals.next_level = EnumLevels.KubirriPage
        elif self.selected_button == "kbbutton":
            Globals.next_level = EnumLevels.KubirriAndWurrumbuPage
        elif self.selected_button == "quiz_button":
            Globals.next_level = EnumLevels.QuizPage
        else:
            Globals.next_level = EnumLevels.ManjalPage

        self.room.running = False

    def key_pressed(self, key):
        if key[pygame.K_RIGHT]:
            self.right_press()
        elif key[pygame.K_LEFT]:
            self.left_press()
        elif key[pygame.K_DOWN]:
            self.down_press()
        elif key[pygame.K_UP]:
            self.up_press()
        elif key[pygame.K_RETURN]:
            self.select_press()

    def joy_pad_signal(self, p1_buttons, p2_buttons):
        if p1_buttons[11] > 0.5:
            self.right_press()
        elif p1_buttons[11] < -0.5:
            self.left_press()
        elif p1_buttons[10] < -0.5:
            self.up_press()
        elif p1_buttons[10] > 0.5:
            self.down_press()
        elif p1_buttons[1]:
            self.select_press()
