from GameFrame import RoomObject, Globals
import os


class EnemyFullBullet(RoomObject):
    def __init__(self, room, x, y, direction):
        RoomObject.__init__(self, room, x, y)

        self.width = 36
        self.height = 64

        self.image = self.load_image(os.path.join('Fight', 'tempEnemyBullet.png'))
        self.set_image(self.image, self.width, self.height)

        self.direction = direction
        self.velocity = 15
        self.forwardCount = 15
        self.rotateCount = 0
        self.splitting = False

        self.register_collision_object('Player')

        self.reRotate()

    def initialForward(self):
        if self.x + self.width < Globals.SCREEN_WIDTH and self.x > 0 and self.y + self.height < Globals.SCREEN_HEIGHT and self.y > 0:
            if self.forwardCount > 0:
                if self.direction == 'left':
                    self.x -= self.velocity
                if self.direction == 'right':
                    self.x += self.velocity
                if self.direction == 'up':
                    self.y -= self.velocity
                if self.direction == 'down':
                    self.y += self.velocity
                self.forwardCount -= 1
            elif not self.splitting:
                self.splitting = True
        else:
            self.room.splitEnemyBullet(self.rect.centerx, self.rect.centery)
            self.delete_object(self)

    def reRotate(self):
        if self.direction == 'left':
            self.rotate(90)
        if self.direction == 'down':
            self.rotate(180)
        if self.direction == 'right':
            self.rotate(270)

    def deleteSelf(self):
        self.delete_object(self)

    def handle_collision(self, other, other_type):
        if other_type == 'Player' and not self.room.Player.invulnerable:
            self.set_timer(1, self.deleteSelf)

    def step(self):
        self.initialForward()
        if self.splitting:
            if self.rotateCount < 20:
                self.rotateCount += 1
                self.rotate(24)
            else:
                self.room.splitEnemyBullet(self.rect.centerx, self.rect.centery)
                self.delete_object(self)
