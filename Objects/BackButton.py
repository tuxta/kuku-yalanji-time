from GameFrame import RoomObject, Globals, EnumLevels
import os
import pygame


class BackButton(RoomObject):
    def __init__(self, room, x, y, wait_time):
        RoomObject.__init__(self, room, x, y)

        image = self.load_image(os.path.join('InfoPage', 'BackButton.png'))

        self.set_image(image, 250, 175)

        self.handle_key_events = True

        self.can_press = False

        self.set_timer(wait_time, self.allow_press)

    def joy_pad_signal(self, p1_buttons, p2_buttons):
        if self.can_press:
            if p1_buttons[2]:
                Globals.next_level = EnumLevels.InfoPage
                self.room.running = False

    def key_pressed(self, key):
        if self.can_press:
            if key[pygame.K_SPACE]:
                Globals.next_level = EnumLevels.InfoPage
                self.room.running = False

    def allow_press(self):
        self.can_press = True
