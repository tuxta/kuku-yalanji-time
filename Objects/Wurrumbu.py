from GameFrame import RoomObject
import os

class Wurrumbu(RoomObject):
    def __init__(self, x, y, room):
        RoomObject.__init__(self, x, y, room)
        self.scale = 4
        self.width = 31 * self.scale
        self.height = 20 * self.scale
        self.animation_folder = "Escape"
        self.set_image(self.load_image(os.path.join("Escape","Wurrumbu Flying 1.png")), self.width, self.height)
        self.depth = 20000
        self.flying_anim = ["Wurrumbu Flying 1.png",
                            "Wurrumbu Flying 2.png"]
        self.animation = self.flying_anim
        self.playing = True