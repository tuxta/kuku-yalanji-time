from GameFrame import RoomObject
import os


class EnemySplitBullet(RoomObject):
    def __init__(self, room, x, y, direction):
        RoomObject.__init__(self, room, x, y)

        self.width = 27
        self.height = 50

        self.image = self.load_image(os.path.join('Fight', 'tempEnemyBulletSplit.png'))
        self.set_image(self.image, self.width, self.height)

        self.direction = direction
        self.velocity = 20

        self.register_collision_object('Player')

        self.reRotate()

    def reRotate(self):
        if self.direction == 'left':
            self.rotate(90)
        if self.direction == 'down':
            self.rotate(180)
        if self.direction == 'right':
            self.rotate(270)
        if self.direction == 'leftdown':
            self.rotate(135)
        if self.direction == 'leftup':
            self.rotate(45)
        if self.direction == 'rightdown':
            self.rotate(225)
        if self.direction == 'rightup':
            self.rotate(315)

    def move(self):
        if self.direction == 'left':
            self.x -= self.velocity
        if self.direction == 'right':
            self.x += self.velocity
        if self.direction == 'up':
            self.y -= self.velocity
        if self.direction == 'down':
            self.y += self.velocity
        if self.direction == 'leftup':
            self.x -= (self.velocity * 0.707)
            self.y -= (self.velocity * 0.707)
        if self.direction == 'leftdown':
            self.x -= (self.velocity * 0.707)
            self.y += (self.velocity * 0.707)
        if self.direction == 'rightup':
            self.x += (self.velocity * 0.707)
            self.y -= (self.velocity * 0.707)
        if self.direction == 'rightdown':
            self.x += (self.velocity * 0.707)
            self.y += (self.velocity * 0.707)

    def deleteSelf(self):
        self.delete_object(self)

    def handle_collision(self, other, other_type):
        if other_type == 'Player' and not self.room.Player.invulnerable:
            self.set_timer(1, self.deleteSelf)

    def step(self):
        self.move()

        if self.x < 0 or self.x > 1280:
            self.delete_object(self)
        elif self.y < 0 or self.y > 720:
            self.delete_object(self)
