from GameFrame import TextObject
import time, pygame


class FPSCounter(TextObject):
    def __init__(self, room, x, y):
        TextObject.__init__(self, room, x, y)
        self.size = 20
        self.colour = (255, 255, 255)
        self.fps = 0
        self.text = ""
        self.start_time = time.perf_counter()
        self.end_time = 0

    def step(self):
        self.end_time = time.perf_counter()

        # use time since the last tick to calculate framerate
        self.fps = 1 / (self.end_time - self.start_time)
        self.update_text_custom()

        self.start_time = time.perf_counter()

    def update_text_custom(self):
        if self.room.__class__.__name__ == "EscapeLevel":
            self.text = f"FPS: {round(self.fps)} | Speed: {round(self.room.move_speed)} | Stage Distance: {round(self.room.stage_dist)} | Obstacles: {self.room.obstacle.obstacles}"
        else:
            self.text = f"FPS: {round(self.fps)}"
        self.built_font = pygame.font.Font('Objects/VCR_OSD_MONO.ttf', self.size)
        self.rendered_text = self.built_font.render(self.text, True, self.colour)
        self.image = self.rendered_text
        self.width, self.height = self.built_font.size(self.text)
        self.rect = pygame.Rect(self.x, self.y, self.width, self.height)