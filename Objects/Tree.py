from GameFrame import RoomObject
import os


class Tree(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        image = self.load_image(os.path.join('Foraging', 'Tree_2.png'))
        self.set_image(image, 202, 250)
