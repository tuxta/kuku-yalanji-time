import pygame, os
from GameFrame import RoomObject, Globals


class Playerdk(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        playerdk = self.load_image(os.path.join('DK', 'front_1.png'))
        self.set_image(playerdk, 32, 48)

        # Load player animation images
        self.down = []
        self.down.append(self.load_image(os.path.join('DK', 'front_1.png')))
        self.down.append(self.load_image(os.path.join('DK', 'front_2.png')))
        self.down.append(self.load_image(os.path.join('DK', 'front_1.png')))
        self.down.append(self.load_image(os.path.join('DK', 'front_2.png')))
        self.up = []
        self.up.append(self.load_image(os.path.join('DK', 'back_1.png')))
        self.up.append(self.load_image(os.path.join('DK', 'back_2.png')))
        self.up.append(self.load_image(os.path.join('DK', 'back_1.png')))
        self.up.append(self.load_image(os.path.join('DK', 'back_2.png')))
        self.left = []
        self.left.append(self.load_image(os.path.join('DK', 'left_1.png')))
        self.left.append(self.load_image(os.path.join('DK', 'left_2.png')))
        self.left.append(self.load_image(os.path.join('DK', 'left_1.png')))
        self.left.append(self.load_image(os.path.join('DK', 'left_2.png')))
        self.right = []
        self.right.append(self.load_image(os.path.join('DK', 'right_1.png')))
        self.right.append(self.load_image(os.path.join('DK', 'right_2.png')))
        self.right.append(self.load_image(os.path.join('DK', 'right_1.png')))
        self.right.append(self.load_image(os.path.join('DK', 'right_2.png')))

        self.img_index = 0

        self.LEFT = 0
        self.RIGHT = 1
        self.UP = 2
        self.DOWN = 3

        self.facing = 4

        self.handle_key_events = True

        self.register_collision_object('DK_Block')
        self.register_collision_object('DK_Boulder')
        self.register_collision_object('DK_Tree')
        self.register_collision_object('DK_Stone')
        self.register_collision_object('MountainPeak')

        self.block_right = False
        self.block_left = False
        self.block_up = False
        self.block_down = False

        self.moving = False
        self.animate()

        self.invulnerable = False
        self.health = 3

    def prestep(self):
        self.block_right = False
        self.block_left = False
        self.block_up = False
        self.block_down = False
        self.facing = 6

    def handle_collision(self, other, other_type):
        if other_type == 'DK_Block':
            if self.collides_at(self, 4, 0, 'DK_Block'):
                self.block_right = True
                if self.x < 596:
                    self.x = self.prev_x
                else:
                    self.move_left()

            if self.collides_at(self, -4, 0, 'DK_Block'):
                self.block_left = True
                if self.x >= 206:
                    self.x = self.prev_x
                else:
                    self.move_right()

            if self.collides_at(self, 0, 4, 'DK_Block'):
                self.block_down = True
                if self.y <= 446:
                    self.y = self.prev_y
                else:
                    self.move_up()

            if self.collides_at(self, 0, -4, 'DK_Block'):
                self.block_up = True
                if self.y >= 250:
                    self.y = self.prev_y
                else:
                    self.move_down()
        elif other_type == 'DK_Boulder':
            self.takeDamage()
        elif other_type == 'DK_Tree':
            if self.collides_at(self, 4, 0, 'DK_Tree'):
                self.block_right = True
                if self.x < 596:
                    self.x = self.prev_x
                else:
                    self.move_left()

            if self.collides_at(self, -4, 0, 'DK_Tree'):
                self.block_left = True
                if self.x >= 206:
                    self.x = self.prev_x
                else:
                    self.move_right()

            if self.collides_at(self, 0, 4, 'DK_Tree'):
                self.block_down = True
                if self.y <= 446:
                    self.y = self.prev_y
                else:
                    self.move_up()

            if self.collides_at(self, 0, -4, 'DK_Tree'):
                self.block_up = True
                if self.y > 250:
                    self.y = self.prev_y
                else:
                    self.move_down()
        elif other_type == 'DK_Stone':
            if self.collides_at(self, 4, 0, 'DK_Stone'):
                self.block_right = True
                if self.x < 1280:
                    self.x = self.prev_x
                else:
                    self.move_left()

            if self.collides_at(self, -4, 0, 'DK_stone'):
                self.block_left = True
                if self.x >= 206:
                    self.x = self.prev_x
                else:
                    self.move_right()

            if self.collides_at(self, 0, 4, 'DK_Stone'):
                self.block_down = True
                if self.y <= 446:
                    self.y = self.prev_y
                else:
                    self.move_up()

            if self.collides_at(self, 0, -4, 'DK_Stone'):
                self.block_up = True
                if self.y > 250:
                    self.y = self.prev_y
                else:
                    self.move_down()
        elif other_type == 'MountainPeak':
            if Globals.animals < 5:
                self.y += 10
            else:
                self.room.end_room()
                self.room.running = False

    def key_pressed(self, key):
        if key[pygame.K_LEFT]:
            self.move_left()
            self.facing = self.LEFT
        if key[pygame.K_RIGHT]:
            self.move_right()
            self.facing = self.RIGHT
        if key[pygame.K_UP]:
            self.move_up()
            self.facing = self.UP
        if key[pygame.K_DOWN]:
            self.move_down()
            self.facing = self.DOWN

    def joy_pad_signal(self, p1_buttons, p2_buttons):
        if p1_buttons[11] < -0.5:
            self.move_left()
        if p1_buttons[11] > 0.5:
            self.move_right()
        if p1_buttons[10] < -0.5:
            self.move_up()
        if p1_buttons[10] > 0.5:
            self.move_down()

    def move_right(self):
        if self.x < Globals.SCREEN_WIDTH - 64:
            self.x += Globals.move_speed

    def move_left(self):
        if self.x > 32:
            self.x -= Globals.move_speed

    def move_up(self):
        if self.y > 250:
            self.y -= Globals.move_speed
        else:
            self.room.shift_room_down()

    def move_down(self):
        if self.y < 450:
            self.y += Globals.move_speed
        else:
            self.room.shift_room_up()

    def animate(self):
        self.img_index += 1
        self.img_index %= 4
        if self.facing == self.LEFT:
            self.set_image(self.left[self.img_index], 32, 48)
        elif self.facing == self.RIGHT:
            self.set_image(self.right[self.img_index], 32, 48)
        elif self.facing == self.UP:
            self.set_image(self.up[self.img_index], 32, 48)
        elif self.facing == self.DOWN:
            self.set_image(self.down[self.img_index], 32, 48)
        else:
            self.set_image(self.down[0], 32, 48)

        self.set_timer(3, self.animate)

    def takeDamage(self):
        if not self.invulnerable:
            self.invulnerable = True
            self.health -= 1
            if self.health <= 0:
                self.room.end_room()
            self.set_timer(30, self.resetDamage)

    def resetDamage(self):
        self.invulnerable = False
