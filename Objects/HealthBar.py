from GameFrame import RoomObject
import os


class HealthBar(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        self.width = 200
        self.height = 64
        self.depth = 10

        # - Loads images in and stores them in a list for later reference - #
        self.noHealth = self.load_image(os.path.join('Fight', 'Missing All Health.png'))
        self.oneHealth = self.load_image(os.path.join('Fight', 'Missing Two Health.png'))
        self.twoHealth = self.load_image(os.path.join('Fight', 'Missing One Health.png'))
        self.fullHealth = self.load_image(os.path.join('Fight', 'Full Health.png'))
        self.images = [self.noHealth, self.oneHealth, self.twoHealth, self.fullHealth]

        self.set_image(self.images[self.room.Player.health], self.width, self.height)

    def step(self):
        self.set_image(self.images[self.room.Player.health], self.width, self.height)
