from Objects.Collectible import Collectible
from Objects.CollectibleIconDK import CollectibleIconDK
from GameFrame import Globals


class CollectibleDK(Collectible):
    def __init__(self, room, x, y, img_type):
        Collectible.__init__(self, room, x, y, img_type)

        self.register_collision_object('Playerdk')

    def handle_collision(self, other, other_type):
        if not self.in_collision:
            Globals.FaunaFlora_found += 1
            self.room.collectible_sound.play()

            icon = CollectibleIconDK(self.room, Globals.icon_bar_length, 40, self.img_file)
            self.room.add_room_object(icon)
            Globals.icon_bar_length += 52

            self.delete_object(self)
            self.x = self.prev_x
            self.y = self.prev_y
            self.in_collision = True
            self.set_timer(4, self.reset_collision)
