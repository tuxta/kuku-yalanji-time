from GameFrame import RoomObject


class TitlePlayButton(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        self.selected_image = self.load_image("ray_button_selected_play.png")
        self.unselected_image = self.load_image("ray_button_unselected.png")

        self.set_image(self.selected_image, 256, 256)

    def set_selected(self, selected):
        if selected:
            self.set_image(self.selected_image, 256, 256)
        else:
            self.set_image(self.unselected_image, 256, 256)
