from GameFrame import RoomObject, Globals
import os


class MountainPeak(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        image = self.load_image(os.path.join("DK", "peak.png"))
        self.set_image(image, Globals.SCREEN_WIDTH, 260)
