from GameFrame import RoomObject
import os


class ForagerMap(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        image = self.load_image(os.path.join('Foraging', 'ForagerMap.png'))
        self.length = 2080
        self.width = 3840
        self.depth = -100
        self.set_image(image, self.width, self.length)
