import os
from GameFrame import RoomObject, Globals


class BackgroundScroll(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        image = self.load_image((os.path.join('DK', 'Background.png')))
        self.set_image(image, Globals.SCREEN_WIDTH, Globals.SCREEN_HEIGHT)
        self.depth = -1000
