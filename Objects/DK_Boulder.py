from GameFrame import RoomObject
from GameFrame import Globals
import random


class DK_Boulder(RoomObject):
    def __init__(self, room, x, y, boulder_image, speed, points):
        RoomObject.__init__(self, room, x, y)

        self.boulder_image = boulder_image
        self.set_image(self.boulder_image, 64, 64)
        self.speed = speed
        self.points = points
        self.reset()

        self.depth = -100

        self.handle_mouse_events = True

        self.register_collision_object('DK_Tree')
        self.register_collision_object('DK_Stone')

    def handle_collision(self, other, other_type):
        if other_type == 'DK_Tree':
            if self.collides_at(self, 4, 0, 'DK_Tree'):
                self.x += 40
        elif other_type == 'DK_Stone':
            if self.collides_at(self, 4, 0, 'DK_Stone'):
                self.x -= 40

    def step(self):
        if self.y > Globals.SCREEN_HEIGHT:
            self.reset()

    def reset(self):
        self.set_direction(90, self.speed)
        self.y = -(random.randint(self.height, 200 + self.height))
        self.x = random.randint(0, Globals.SCREEN_WIDTH - self.width)
        self.set_image(self.boulder_image, 64, 64)
