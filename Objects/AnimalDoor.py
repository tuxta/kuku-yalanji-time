from GameFrame import RoomObject
import os


class AnimalDoor(RoomObject):
    def __init__(self, room, x, y,):
        RoomObject.__init__(self, room, x, y)

        image = (self.load_image(os.path.join('DK','front_1.png')))

        self.set_image(image, 32, 32)

        # self.register_collision_object("Player")
