from GameFrame import RoomObject
import os


class No(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        self.selected_image = self.load_image(os.path.join("InfoPage", "FalseHighlight.png"))
        self.unselected_image = self.load_image(os.path.join("InfoPage", "False.png"))

        self.set_image(self.unselected_image, 200, 200)

        self.handle_key_events = True

        self.selected = False

    def set_selected(self, selected):
        if selected:
            self.set_image(self.selected_image, 200, 200)
            self.selected = True
        else:
            self.set_image(self.unselected_image, 200, 200)
            self.selected = False

