from GameFrame import RoomObject
import os


class DK_Tree(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        image = self.load_image(os.path.join('DK', 'Tree.png'))
        self.set_image(image, 112, 128)


