from GameFrame import RoomObject, Globals
import pygame


class InfoPopup(RoomObject):
    def __init__(self, room, x, y, image):
        RoomObject.__init__(self, room, x, y)

        self.set_image(
            image,
            Globals.SCREEN_WIDTH - 100,
            Globals.SCREEN_HEIGHT - 100
        )

        self.handle_key_events = True

        self.depth = 200

    def joy_pad_signal(self, p1_buttons, p2_buttons):
        if p1_buttons[1]:
            self.delete_object(self)
            Globals.popup_active = False
            if Globals.FaunaFlora_found == Globals.FaunaFlora_count:
                self.room.end_room()

    def key_pressed(self, key):
        if key[pygame.K_q]:
            self.delete_object(self)
            Globals.popup_active = False
            if Globals.FaunaFlora_found == Globals.FaunaFlora_count:
                self.room.end_room()
