from GameFrame import RoomObject
import pygame
import os


class Instructions(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        self.instruction_image = self.load_image(os.path.join("Escape", "Instructions.png"))
        self.set_image(self.instruction_image, 1280, 720)
        self.handle_key_events = True

        self.can_continue = False

        self.set_timer(30, self.enable_continue)

    def key_pressed(self, key):
        if key[pygame.K_SPACE]:
            if self.can_continue:
                self.room.running = False

    def joy_pad_signal(self, p1_buttons, p2_buttons):
        if p1_buttons[1]:
            if self.can_continue:
                self.room.running = False

    def enable_continue(self):
        self.can_continue = True
