from GameFrame import RoomObject
import os


class Crocodile(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        image = self.load_image(os.path.join('DK','Crocodile.png'))
        self.set_image(image, 64, 64)
