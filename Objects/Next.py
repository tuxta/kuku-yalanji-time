from GameFrame import RoomObject, Globals, EnumLevels
import pygame
import os


class Next(RoomObject):
    def __init__(self, room, x, y):
        RoomObject.__init__(self, room, x, y)

        image = self.load_image(os.path.join('InfoPage', "NextButton.png"))
        self.set_image(image, 300, 225)

        self.handle_key_events = True

    def joy_pad_signal(self, p1_buttons, p2_buttons):
        if p1_buttons[2]:
            Globals.next_level = EnumLevels.InfoPage
            self.room.running = False

    def key_pressed(self, key):
        if key[pygame.K_SPACE]:
            Globals.next_level = EnumLevels.InfoPage
            self.room.running = False
