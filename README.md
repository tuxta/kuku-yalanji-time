# Kuku Yalanji Time

The software provides interactive story telling of the Kuku Yalanji Dreaming story of Manjal Dimbi, Kuburri and Wurrumbu.


## Authors and acknowledgment
This software was developed by year 9 and 10 high school students (using GameFrame) for the Kuku Yalanji People of North Queensland.

## License
This project is Licensed Under the GNU General Public license Version 3



