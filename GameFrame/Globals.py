from enum import IntEnum


class EnumLevels(IntEnum):
    TitleRoom = 0
    InfoPage = 1
    Controls = 2
    About = 3
    WurrumbuPage = 4
    ManjalPage = 5
    KubirriPage = 6
    QuizPage = 7
    FinishQuiz = 8
    KubirriAndWurrumbuPage = 9
    StoryIntro = 10
    StoryMid = 11
    StoryWurrumbu = 12
    StoryEnd = 13
    ForagingRoom = 14
    EscapeLevelIntro = 15
    EscapeLevelInstructions = 16
    EscapeLevel = 17
    EscapeLevelOutro = 18
    FightRoom = 19
    StoryClimb = 20
    DonkeyKong_Controls = 21
    DonkeyKong = 22
    LevelSelect = 23


class Globals:

    running = True
    FRAMES_PER_SECOND = 30

    SCREEN_WIDTH = 1280
    SCREEN_HEIGHT = 720

    SCORE = 0

    # - Set the starting number of lives - #
    LIVES = 3

    # - Set the Window display name - #
    window_name = 'Kuku Yalanji Time'

    # Set to true to enable level select and other stuff
    # Don't keep this set to true in the final build pls that would be bad
    debug = False

    # - Set the order of the rooms - #
    levels = [
        "TitleRoom",
        "InfoPage",
        "Controls",
        "About",
        "WurrumbuPage",
        "ManjalPage",
        "KubirriPage",
        "QuizPage",
        "FinishQuiz",
        "KubirriAndWurrumbuPage",
        "StoryIntro",
        "StoryMid",
        "StoryWurrumbu",
        "StoryEnd",
        "ForagingRoom",
        "EscapeLevelIntro",
        "EscapeLevelInstructions",
        "EscapeLevel",
        "EscapeLevelOutro",
        "FightRoom",
        "StoryClimb",
        "DonkeyKong_Controls",
        "DonkeyKong",
        "LevelSelect"
    ]

    # - Set the starting level - #
    if debug:
        start_level = EnumLevels.LevelSelect
    else:
        start_level = EnumLevels.TitleRoom

    # - Set this number to the level you want to jump to when the game ends - #
    end_game_level = EnumLevels.TitleRoom

    # - This variable keeps track of the room that will follow the current room - #
    # - Change this value to move through rooms in a non-sequential manner - #
    next_level = 0

    # - Change variable to True to exit the program - #
    exiting = False


# ############################################################# #
# ###### User Defined Global Variables below this line ######## #
# ############################################################# #

    total_count = 0
    destroyed_count = 0
    move_speed = 4
    FaunaFlora_count = 10
    tree_count = 0

    popup_active = False
    tree_spawning = False

    FaunaFlora_found = 0
    icon_bar_length = 40

    num_Boulders = 0
    animals = 0
